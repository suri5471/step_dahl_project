To replicate the results one only has to:

1) Make sure that Stata and Anaconda are installed and are added to the PATH. Both steps are nicely 
explained in this Tutorial: (http://hmgaudecker.github.io/econ-python-environment/index.html) for Windows, 
Mac and Linux.

2) Make sure a LaTeX distribution (i.e. Texlive (https://www.tug.org/texlive/) and Biber
 (http://biblatex-biber.sourceforge.net/) are installed. For problems with biber see here:
 (http://hmgaudecker.github.io/econ-project-templates/faq.html#latex-waf).

3) Open a Shell in the root directory of the project and type:
    * python waf.py configure
    * python waf.py (this is the main step and can take rather long)
    * python waf.py install

The stata code was written for version 13.1. However, we didn't include the version statements in the 
programs, such that it can be run with older versions as well, as long as they support the used commands.
This should be the case with version 11.1 and newer.

At the time of writing the code, there was a bug in Stata 13.1 under Windows that led to errors when using
the ``mprobit`` and ``mlogit`` commands.  A simple workarround (without any guarantee) seems to be Stata's
version control with version 10 before each of these commands. As the rest of our code is not guaranteed to
work with version 10, the version should be reset to something higher than 11.1 after the command. The
Linux version of Stata doesn't have this problem. The mac version was not checked.

The number of bootstrab replications is set to 2. This is only sufficient for a test if the code runs through. In the paper we used 400 replications. To change the number of bootstrap replications one just has to change it once in src.model_specs.bs_rep.json.