import json
from bld.project_paths import project_paths_join as ppj
from src.library.dofile_write_functions import *


if __name__ == "__main__":
    # load a dictionarly of lists with the independent variable lists for the different models
    with open(ppj("IN_MODEL_SPECS", "independent_variables.json")) as n:
        indep_vars = json.load(n)


    # load the dictionary with the country specific information
    with open(ppj("LIBRARY", "country_information.json")) as m:
        country_dict = json.load(m)
    country_list = sorted(list(country_dict.keys()))

    svyset_dict = {}
    for country in country_list:
        svyset_dict[country] = country_dict[country]['svyset']

    indep_specs_list = indep_vars.keys()
    for spec in indep_specs_list:
        indep_vars[spec]['all_indeps'] = \
            indep_vars[spec]['regression'] + indep_vars[spec]['exclusion']
        with open(ppj('OUT_MODEL_SPECS', 'global_indep_macro_{}.do'.format(spec)), 'w') as dofile:
            dofile.write(dict_to_macros('global', indep_vars[spec], spec))

    with open(ppj('OUT_MODEL_SPECS', 'svyset_info.do'), 'w') as dofile:
        dofile.write(dict_to_macros('global', svyset_dict, 'svy'))

