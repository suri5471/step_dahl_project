{smcl}
{cmd:help bsweights}{right: ({browse "http://www.stata-journal.com/article.html?article=st0187":SJ10-2: st0187})}
{hline}

{title:Title}

{p2colset 5 18 20 2}{...}
{p2col :{hi:bsweights} {hline 2}}Bootstrap weights for complex surveys{p_end}
{p2colreset}{...}


{title:Syntax}

{p 8 17 2}
{cmd:bsweights}
{it:prefix}{cmd:,}
{cmdab:r:eps(}{it:#}{cmd:)}
{cmd:n(}{it:#}{cmd:)}
[{cmd:replace}
{cmdab:ave:rage(}{it:#}{cmd:)}
{cmdab:bal:anced}
{cmd:dots}
{cmdab:cal:ibrate(}{it:call_to_weight_calibration_routine}{cmd:)}
{cmdab:verb:ose}
{cmd:nosvy seed(}{it:#}{cmd:)} {cmdab:flo:at}
{cmdab:dou:ble}]

{pstd}Requires the {helpb bs4rw} command. If not available in your system,
    {net "describe http://www.stata.com/users/jpitblado/bs4rw":install from here}.


{title:Description}

{pstd}
{cmd:bsweights} provides the bootstrap resampling weights to be used by 
{helpb bs4rw} with arbitrary estimation commands. It is primarily intended to
be used with complex surveys, though some nonsurvey uses can also be
suggested.

{pstd}
Typical use of {cmd:bsweights} is

{phang2}{cmd:bsweights bsw, n(}{it:#}{cmd:) reps(}{it:#}{cmd:) ...}{p_end}
{phang2}{cmd:bs4rw, rw(}{it:bsw*}{cmd:): }{it:estimation_command}{cmd: [pw=}{it:original_weight}{cmd:]}

{pstd} Do not forget the original weight variable to be specified with 
{helpb bs4rw} or else your point estimates will be incorrect.

{pstd} Suppose in a complex survey design (specified by {helpb svyset}), there
are {it:L} strata and {it:n_h} units in strata {it:h}=1,...,{it:L}. To ensure
that the bootstrapping scheme resembles the original sampling scheme, you need
to resample all PSUs within strata. Moreover, in typical survey settings with
small values of {it:n_h}, small-sample bootstrap biases arise, and the
bootstrap replicates need to be rescaled to obtain consistent estimates of
variance. Rao and Wu (1988) demonstrated that a na{c i:}ve bootstrap that
resamples {it:m_h} units from the {it:h}th stratum is inconsistent,
underestimating the true variance, and they proposed internal scaling to
restore consistency. Rao, Wu, and Yue (1992) explained how weights rather than
estimates themselves can be rescaled, which is by far more convenient in
computational terms.

{pstd} Thus the role of {cmd:bsweights} is to change the mechanics of the
bootstrap:  instead of resampling the data {it:R} times from memory, {it:R}
sets of replicate weights are constructed. Then {helpb bs4rw} performs
multiple calls to the command of interest, substituting {cmd:pweight} for the
{it:r}th replicate weight for {it:r}=1,...,{it:R}. To that effect,
{cmd:bsweights} is a pre-estimation command.

{pstd} There are several advantages of the bootstrap over other methods of
survey inference. First, like with other replication methods (e.g., balanced
repeated replication, jackknife), the bootstrap does not require any
command-specific programming and is applicable to a wide range of estimation
commands.  {cmd:bsweights} together with {helpb bs4rw} can further extend this
range beyond those estimation commands supported by {helpb svy estimation}.
Second, releasing the sets of replication weights rather than the unit
identifiers improves confidentiality protection. Third, the bootstrap method
provides consistent variance estimates in case of nonsmooth statistics such as
distribution quantiles. However, the bootstrap is computationally intensive,
and the resulting variance estimates tend to be less stable than with other
methods.


{title:Options}

{phang}{cmd:reps(}{it:#}{cmd:)} specifies the number of bootstrap replications
to be taken and the number of weight variables to be generated. 
{cmd:reps()} is required.

{phang}{cmd:n(}{it:#}{cmd:)} specifies how the number of PSUs {it:m_h} per
stratum be handled. If a positive number is specified, it is interpreted as
the number of units per stratum {it:m_h} that will be used. If a nonpositive
number is specified, then m_h = n_h - |{it:#}|.  Specifying {bf:n(0)} leads to
the number of units resampled being equal to the original number of units,
{it:n_h}. {cmd:n()} is required.

{phang}{cmd:replace} requests that the weight variables be created anew.  Use
with caution; it will {cmd:drop} the existing {it:prefix*} variables!

{phang}{cmd:average(}{it:#}{cmd:)} implements the mean bootstrap. The
bootstrap frequency counts are averaged across the given number of
replications. If the bootstrap weights are created using this option, you
should specify the {cmd:vfactor()} option of {cmd:bs4rw}. The total number of
replications is the product of {cmd:reps()} and {cmd:average()}.

{phang}{cmd:balanced} requests the balanced bootstrap (Graham et al. 1990).

{phang}{cmd:dots} provides additional output.

{phang}{cmd:calibrate(}{it:call_to_weight_calibration_routine}{cmd:)} allows a
call to another program to adjust the weights for poststratification and
nonresponse.

{phang}{cmd:verbose} provides output from the weights calibration commands.

{phang}{cmd:nosvy} explicitly states that the data are not of the survey
format.

{phang}{cmd:seed(}{it:#}{cmd:)} specifies the seed for the random-number
generator. See {helpb set seed}.

{phang}{cmd:float} specifies that the weight variables have float type. See
{helpb data types}.

{phang}{cmd:double} specifies that the weight variables have double type. See
{helpb data types}.


{title:Remarks}

{pstd}Remarks are presented under the following headings:

{phang2}{help bsweights##mh:Sample sizes}{p_end}
{phang2}{help bsweights##balance:Achieving balance}{p_end}
{phang2}{help bsweights##calibr:Calibration}{p_end}
{phang2}{help bsweights##nonsvy:Other applications}{p_end}


{marker mh}{...}
{title:Sample sizes}

{pstd} One of the main parameters of the survey bootstrap procedure is the
bootstrap sample size {it:m_h}, that is, the number of resampled units per
stratum. Weight scaling ensures that the variance estimator is consistent for
any value of {it:m_h}, but some additional performance gains can be obtained
by choosing {it:m_h} carefully.

{pstd} First, the number of resampled units must be at least one in every
stratum. {cmd:bsweights} will issue an error message and abort if {it:m_h} is
less than or equal to zero in any strata.

{pstd} Second, the choice {it:m_h} > {it:n_h}-1 can lead to some problems of
the range of weights. If the estimated parameter is variance, correlation, or
cumulative distribution function, then estimates outside of the natural range
can be obtained in some bootstrap samples, which is most likely undesirable.

{pstd} Rao and Wu (1988) suggested that, in the case of known variances, the
sample size {it:m_h}=({it:n_h}-2)^2/({it:n_h}-1) will match the third moments
of the distribution of the estimated parameter and thus capture the
corresponding term in Edgeworth expansion. For {it:n_h} > 4, this essentially
boils down to {cmd:n(-3)}.


{marker balance}{...}
{title:Achieving balance}

{pstd}The idea of the balanced bootstrap (Graham et al. 1990) is that in a
given stratum, each unit is repeated the same number of times across all
replications.  Hence, for each stratum {it:h}, the total number of units used
in all replications, {it:R*m_h}, must be divisible by {it:n_h}.

{pstd}For instance, if the design features the strata sizes 2, 3, 4, and 5,
then the minimal number of replications where the balance can be achieved is
{it:R}=3*4*5=60. Other good values of {it:R} will be multiples of this
smallest common multiple: 120, 180, 240, 300, 480, and so on. The bootstrap
sample sizes of {it:m_h}=2, 3, 4, or 5 can all be used.

{pstd}As another example, if the design features strata sizes {it:n_h}=7, 8,
and 10, and it is desirable to use the {it:m_h=n_h}-3 rule giving resampled
sizes of 4, 5, and 7, then the smallest "good" number of bootstrap
replications is given by 280, which can be divided by all of 7, 8, and 10.
Each unit from strata with 7 PSUs is used 280*4/7 = 160 times; from strata
with 8 PSUs, 280*5/8 = 175 times; and from strata with 10 PSUs, 280*7/10=196
times.

{marker calibr}{...}
{title:Calibration}

{pstd}The format of the {cmd:calibrate} option is as follows:

{phang2}{cmd:calibrate(}[{cmd:do}] {it:calibration_program }[{it:arg1}] {cmd:@} [{it:arg2}]{cmd:)}

{pstd}When rescaling the weights, {cmd:bsweights} makes a call to
{it:calibration_program}, substituting {cmd:@} for the names corresponding to
the weights being generated and carrying over optional parts of the syntax
{it:arg1}, {it:arg2}. For instance, if you specify

{phang2}{cmd:bsweights bsw, ... calibrate( do adjust @ )}

{pstd}then {cmd:bsweights} will issue the consecutive commands

{phang2}{cmd:do adjust bsw1}{p_end}
{phang2}{cmd:do adjust bsw2}{p_end}
{phang2}{cmd:...}{p_end}

{pstd}In turn, the do-file {cmd:adjust.do} will typically contain statements
like

{phang2}{cmd:args weight_var}{p_end}
{phang2}{cmd:...}{p_end}
{phang2}{cmd:replace `weight_var' = ...}{p_end}
{phang2}{cmd:...}{p_end}

{pstd}The adjustments are taking place after Rao, Wu, and Yue's (1992)
scaling.  You are responsible for providing correct treatment of resampling
weights in your calibration procedures.  The {cmd:verbose} option provides
calibration output for debugging purposes.

{pstd}For proper results, the survey agency must provide the original
probability weights as inputs to {cmd:bsweights}. The same adjustment
procedure that produces the publicly available weights from those probability
weights should be applied to the bootstrap weights.

{pstd}Generally, combining calibration and the mean bootstrap may lead to
incorrect results. An exception to this rule is calibration for domain
estimation.


{marker nonsvy}{...}
{title:Other applications}

{pstd}The nonsurvey applications of {cmd:bsweights} include accurate bias
estimation (by using the {cmd:balanced} bootstrap option) and weighted
bootstrap.

{pstd}Bias estimation: Compare the results of{p_end}

{phang2}{cmd:. sysuse auto}{p_end}
{phang2}{cmd:. bootstrap, reps(150): mean mpg}{p_end}
{phang2}{cmd:. estat bootstrap, all}{p_end}

{pstd}and

{phang2}{cmd:. bsweights bsw, reps(148) n(50) balanced nosvy}{p_end}
{phang2}{cmd:. bs4rw, rw(bsw*): mean mpg}{p_end}
{phang2}{cmd:. estat bootstrap, all}{p_end}


{title:Examples}

{pstd}Design variance of correlation coefficient{p_end}
{phang2}{cmd:. webuse nhanes2, clear}{p_end}
{phang2}{cmd:. bsweights bsw, reps(124) n(1) balanced dots}{p_end}
{phang2}{cmd:. bs4rw corr=r(rho), rweights(bsw*): corr weight height [fw=final]}{p_end}

{pstd}Compare the results: Linearization versus random repeated
replication{p_end}
{phang2}{cmd:. svy: logit highbp weight height age female}{p_end}
{phang2}{cmd:. bs4rw, rweights(bsw*): logit highbp weight height age female [pw=finalwgt]}{p_end}
{phang2}{cmd:. estat bootstrap, all}{p_end}

{pstd}Estimation for subpopulation{p_end}
{phang2}{cmd:. program define SubPopW}{p_end}
{phang2}{cmd:.   gettoken weightvar condition: 0}{p_end}
{phang2}{cmd:.   replace `weightvar' = 0 if !(`condition')}{p_end}
{phang2}{cmd:. end}{p_end}
{phang2}{cmd:. bsweights bsw, reps(124) n(1) calibrate(SubPopW @ black) balanced dots replace}{p_end}
{phang2}{cmd:. bs4rw corr=r(rho), rweights(bsw*): corr weight height [fw=final*black]}{p_end}
{phang2}{cmd:. svy, subpop(black): logit highbp weight height age female}{p_end}
{phang2}{cmd:. bs4rw, rweights(bsw*): logit highbp weight height age female [pw=final*black]}{p_end}

{pstd}Note how weights are modified in the last call to ensure that appropriate
point estimates are posted by {cmd:bs4rw}.


{title:References}

{phang}
    Graham, R. L., D. V. Hinkley, P. W. M. John, and S. Shi. 1990.
    Balanced design of bootstrap simulations.
    {it:Journal of the Royal Statistical Society, Series B} 52: 185-202.

{phang}Rao, J. N. K., and C. F. J. Wu. 1988. Resampling inference with complex
survey data. {it:Journal of the American Statistical Association} 83: 231-241.

{phang}Rao, J. N. K., C. F. J. Wu, and K. Yue. 1992. Some recent work on
resampling methods for complex surveys. {it:Survey Methodology} 18: 209-217.

{phang}Other useful references can be found in
{browse "http://www.citeulike.org/user/ctacmo/tag/survey_resampling":an extended bibliographic list}
   on survey resampling assembled by Stanislav Kolenikov.


{title:Author}

{pstd}Stanislav Kolenikov{p_end}
{pstd}University of Missouri{p_end}
{pstd}Columbia, MO{p_end}
{pstd}kolenikovs@missouri.edu{p_end}


{title:Also see}

{psee}
Article: {it:Stata Journal}, volume 10, number 2: {browse "http://www.stata-journal.com/article.html?article=st0187":st0187}

{psee}
{space 3}Help:  {manhelp svy SVY}, {manhelp svy_brr SVY:svy brr}, 
{manhelp svy_jackknife SVY:svy jackknife}, {manhelp bootstrap R}, {helpb bs4rw} (if installed)
{p_end}
