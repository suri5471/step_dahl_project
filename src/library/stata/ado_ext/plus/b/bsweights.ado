*! bsweights to generate bootstrap weights; v.1.4 -- Stas Kolenikov
program define bsweights, rclass sortpreserve

    version 10

    syntax newvarname, Reps(integer) n(string) ///
         [BALanced QMCStratified QMCMatrix SHUFfle REPLACE noSVY VERBose CALibrate(str) ///
         dots LOGLevel(int 0) DOUble FLOat SEED(str) AVErage(int 1) ]
    local prefix `varlist'
    // syntax: bsweights prefix variable, reps(# repetitions) n(# units)
    local QMC `qmcstratified'`qmcmatrix'
    local gtype `double'`float'

    if "`seed'"~="" set seed `seed'

    // parse svyset
    local strata : char _dta[_svy_strata1]
    if "`strata'" == "" {
       local strata : char _dta[strata]
    }
    if "`strata'" == "" & "`svy'" != "nosvy" {
       di as err "svy is not set"
       exit 119
    }
    if "`svy'" == "nosvy" {
       // set up a fake design
       tempvar stru
       qui g byte `stru' = 1
       local strata `stru'
    }

    local weight : char _dta[_svy_wvar]
    if "`weight'" == "" {
       local weight : char _dta[pweight]
    }
    if "`weight'" == "" local weight 1
    // may not be robust when `weight' is interpreted as a variable?

    local id : char _dta[_svy_su1]
    if "`id'" == "" {
       local id : char _dta[psu]
    }
    if "`id'" == "" | "`id'"=="_n" | "`svy'"=="nosvy" {
       tempvar nid
       qui g long `nid' = _n
       local id `nid'
    }
    // end parsing svyset

    preserve
    // just in case the user breaks while -bsweights- is running

    if `average'>1 & "`calibrate'" ~= "" {
       noi di "{err}Warning: combination of calibration with mean bootstrap can lead to incorrect results"
    }

    if "`dots'"~="" local loglevel 1

    quietly {
       tempvar n_h m_h first mysort
       gen long `mysort' = _n
       bysort `strata' `id' (`_sortindex' `mysort') : g byte `first' = (_n==1)
       by `strata' (`id' `_sortindex' `mysort'): g long `n_h' = sum(`first')
       by `strata' (`id' `_sortindex' `mysort'): replace `n_h' = `n_h'[_N]

       // parse n, i.e., m_h
       cap confirm integer number `n'
       if _rc {
          // `n' is not a number... what can it be?
          if lower("`n'") == "raowu" {
             // Rao & Wu (1998) optimal rule
             gen int `m_h' = round( (`n_h'-2)^2/(`n_h'-1), 1)
          }
          else {
             di as err "don't know how to interpret n(`n')"
             exit 194
          }
       }
       else if `n' > 0 {
          // interpet as the number of units to use
          gen int `m_h' = `n'
       }
       else {
          // negative -- interpret as the number of units to subtract
          gen int `m_h' = `n_h' + `n'
       }

       // check against the existing design
       sort `strata' `m_h'
       cap by `strata' : assert `m_h'[1] == `m_h'[_N]
       if _rc {
          di as err "# of PSUs to bootstrap is not constant within strata"
          exit 194
       }
       cap assert `m_h' >= 1
       if _rc {
          di as err "# of PSUs to bootstrap is not positive in some strata"
          exit 194
       }

       cap assert `m_h' <= `n_h'
       if _rc == 9 {
          noi di "{err}Warning: the number of replicates is greater than" _c
          noi di "{err}the number of available units for some strata{res}\n"
       }
       // that's not good, but if the user wants it, we can deliver

       // renumber the strata to run from 1 to H
       tempvar newstrata diffstrata
       egen int `newstrata' = group(`strata')
       gen `diffstrata' = abs(`newstrata' - `strata')
       sum `diffstrata', mean
       if r(mean) != 0 local strata `newstrata'

       sort `strata' `id' `_sortindex'

       local H = `strata'[_N]

       // to-be weight variables
       // DANGEROUS!!!
       if "`replace'" == "replace" cap drop `prefix'*
       forvalues k=1/`reps' {
          // something strange might happen here with those prefixed variables

          cap g `gtype' `prefix'`k' = .
          local rc = _rc
          if `rc' == 110 & "`replace'"=="" {
              di as err "`prefix'`k' already defined"
              local kk = `k'-1
              cap drop `prefix'1-`prefix'`kk'
              exit 110
          }
          else if `rc' == 902 {
              local kk = `k'-1
              cap drop `prefix'1-`prefix'`kk'
              error 902
          }
          else {
              replace `prefix'`k' = .
          }
       }
       unab preflist : `prefix'1 - `prefix'`reps'

       tempvar bvar

       if "`QMC'" == "" {
         // bootstrap

         if "`balanced'" == "" {

           // create tempvars
           forvalues t = 1/`average' {
             tempvar tw`t'
             gen `tw`t'' = .
             local twlist `twlist' + `tw`t''
           }

           // simple bootstrap
           if `loglevel'>0 {
              if `average'==1 noi di "{txt}Running {help bsample} {res}`reps' {txt} times " _c
              else noi di "{txt}Running bsample {res}`reps'{txt}x{res}`average'  {txt} times " _c
           }

           forvalues k=1/`reps' {
              forvalues t=1/`average' {
                bsample `m_h', strata(`strata') cluster(`id') weight(`tw`t'')
              }
              // `twlist' starts with + sign, need to clear that up
              replace `prefix'`k' = ( 0 `twlist' ) / `average'
              if `loglevel' >= 1 noi di "{txt}." _c
           }

           egen double `bvar' = rsum(`prefix'*)
         }

         else {
              // balanced bootstrap
              g double `bvar' = .
              // send to Mata:
              //   the number of replications to take m_h
              //   the stratum identifier
              //   the unit ID variable
              //   the set of the new weight variables
              //   a temporary variable
              //   the selection variable
              //   the current loglevel
              noi mata : BalancedBootstrapDesign("`m_h'", "`strata'", "`id'", "`preflist'", "`bvar'", "`first'", `average', `loglevel' )
              // we've got the weight variables filled with counts (if `first')
         }

       } // end of the bootstrap

       else if "`QMC'"=="qmcstratified" {
            // within each stratum, generate Halton sequences
            // and count frequencies
            assert `strata' >= 1 & `strata' <= `H'
            cap confirm number `n'
            if _rc | `n' <= 0 {
               di as err "n must be a constant with QMC stratified design"
            }

            g double `bvar' = .

            noi mata : HaltonDesignStratified( `reps', `n', "`preflist'", "`strata'", "`id'", "`first'", `average', `loglevel', "`bvar'", "`shuffle'", "`balanced'" )
       }

       else if "`QMC'"=="qmcmatrix" {
            // generate a design based on 2D representation
            if "`balanced'" == "" local balanced .
            if "`shuffle'"  == "" local shuffle .

            g double `bvar' = .
            noi mata : HaltonDesign2D( `reps', "`m_h'", "`preflist'", "`strata'", "`id'", "`first'", `average', `loglevel', "`balanced'", "`shuffle'", "`bvar'" )
       }
       // end of the flavors

       // the `prefix'* variables now contain the counts of how
       // many times each unit was resampled for a particular replication

       // change the `prefix'* variables from counts to real weights
       if "`calibrate'" ~= "" {
          tokenize `calibrate', parse("@")
          if "`2'" ~= "@" {
             noi di as err "Warning: calibrate option specified improperly, no calibration will be performed"
          }
          else {
             local calfirst `1'
             local callast `3'
          }
          if "`verbose'" ~= "" local verbose cap noi
          else local verbose qui
       }
    } // of quietly

    if `loglevel'>0 noi di _n "{txt}Rescaling weights "
    forvalues k=1/`reps' {
        // expand the weights/counts to all observations; not needed for simple bootstrap though
        qui bysort `strata' `id' (`_sortindex' `mysort') : ///
              replace `prefix'`k' = `prefix'`k'[1]
        // scale the weights
        qui by `strata' `id'  : ///
              replace `prefix'`k' = ( ( 1 - sqrt( `m_h'/(`n_h'-1) ) ) ///
                      + sqrt( `m_h'/(`n_h'-1) )*(`n_h'/`m_h')*`prefix'`k' )*`weight'
        if "`calfirst'" ~= "" {
            `verbose' `calfirst' `prefix'`k' `callast'
        }
        if `loglevel' >= 1 di "{txt}." _c
    }
    if `loglevel'>= 1 di _n

    // `bvar' contains the total counts of how many times each unit was used
    // verify the balance condition
    cap bysort `strata' `first' (`bvar' `_sortindex')  : assert (`bvar'[_N] - `bvar'[1] < c(epsfloat) ) if ~mi(`bvar'[_N])
    return scalar balanced = (_rc == 0)
    if ( (!return(balanced)) & (`loglevel'>=1)) {
        noi di as err "Warning: the first-order balance was not achieved"
    }

    // this is probably extraneous
    sort `strata' `id' `_sortindex' `mysort'
    restore, not

end


mata :

// needs: `reps' `n' `prefix'* designvars?
// returns: `prefix'* filled with counts
void HaltonDesignStratified(
    real scalar reps,    // # replications
    real scalar m_h,     // variable containing m_h -- same for all h!
    string matrix wgt,   // names of the weights varaibles
    string scalar strata, string scalar psu, // design variables
    string scalar first,  // selectvar
    real scalar average,  // # of replications to average across
    | real scalar loglevel,    // loglevel
    string scalar bvar, // balancing variable
    string scalar shuffle,  // version of the QMC procedure
    string scalar balanced  // version of the QMC procedure
) {

    // design information
    // strata numbers are 1, ... , H
    st_view(design=.,.,(tokens(strata), tokens(psu)), tokens(first) )
    n = rows(design)
    H = design[ n, 1 ]

    // get n_h
    n_h = J(H,1,.)
    for (h=1;h<=H;h++) {
       this_h = !( J( n, 1, h ) - design[.,1] )
       st_select(d_h=., design, this_h )
       n_h[h] = rows(d_h)
    }

    // length of the Halton sequence
    L = m_h*reps*average

    // the base sequence

    // get the prime numbers
    prim = Primes(H)

    // form Halton sequence
    Hseq = J(L-1,0,.)
    for(h=1;h<=H;h++) {
       Hseq = Hseq, ghalton(L-1, prim[h], 0)
    }
    Hseq = J(1,H,0) \ Hseq

    // shuffle?
    if (shuffle=="shuffle") {
      // shuffle each of the components
      Hshuf = J(L,0,.)
      for(h=1;h<=H;h++) {
         // shuffle h-th column
         HH = jumble( Hseq[.,h] )
         Hshuf = ( Hshuf, HH )
      }
      Hseq = Hshuf
    } ;

    // pre-process the Halton sequence
    if (balanced=="balanced") {
      for(h=1;h<=H;h++) {
         _sort( Hseq, h )
         for(k=1;k<=L;k++) {
           Hseq[k,h] = floor( (k-1)*n_h[h]/L ) + 1
         }
      }
    }
    else {
      for(h=1;h<=H;h++) {
         for(k=1;k<=L;k++) {
           Hseq[k,h] = floor( Hseq[k,h]*n_h[h]) + 1
         }
      }
    }
    if (loglevel >= 3) Hseq


    // the weight variables
    st_view(weight=.,.,tokens(wgt), tokens(first) )
    weight[,]=J(n,reps,0)

    line = 0
    // cycle over replications
    for(i=1;i<=m_h;i++) {
        // take the next line of the Halton matrix
        for(r=1;r<=reps;r++) {
           // cycle over averaging frame
           for(t=1;t<=average;t++) {
              // cycle over observations within replication
              line++
              for(h=1;h<=H;h++) {
                // convert the Halton seq value to the observation index
                j = Hseq[line,h]
                // get to the weights in the stratum h
                this_h = !(J( n, 1, h ) - design[.,1] )
                st_select(w_h=., weight, this_h)
                // increase counts of the freq weight variable
                w_h[ j,r] = w_h[ j,r] + 1/average
                if (loglevel >=3 ) {
                    printf("{txt}In stratum {res}%3.0f{txt}, {res}%4.0f{txt}-th element", h, line)
                    printf("({res}%8.6f{txt})", Hseq[line,h])
                    printf("{txt} was mapped to [{res}%5.0f{txt}, {res}%5.0f{txt}]\n", j, r)
                }
              }
           }
        }
    }
    if (bvar != "") {
        st_view(bal=.,.,tokens(bvar), tokens(first) )
        bal[,] = rowsum(weight)
        if (loglevel>2) bal
    }

}

void HaltonDesign2D(
    real scalar reps,  // # replications
    string scalar mhvar,     // name of the m_h variable
    string matrix wgt, // names of the weights varaibles
    string scalar strata, string scalar psu, // design variables
    string scalar first,     // select variable
    real scalar average,  // # of replications to average across
    | real scalar loglevel,    // loglevel
    string scalar balance, string scalar shuffle, // flavor of the QMC procedure
    string scalar bvar // tempvar to contain the balancing
) {

    if (loglevel >= 2) {
        printf("{txt}Input parameters: reps = {res}%5.0f{txt}, m_h = {res}%s\n", reps, mhvar)
        printf("{txt}Design variables are {res}%12s{txt} and {res}%12s\n", strata, psu)
        printf("{txt}Flavors are {res}%8s{txt} and {res}%8s\n", balance, shuffle)
    }

    // design information
    // assumed equal # PSU per stratum
    // strata numbers are 1, ... , H
    st_view(design=.,.,(tokens(strata), tokens(psu), tokens(mhvar) ), tokens(first) )
    nobs = rows(design)
    H = design[ nobs, 1 ]

    if (loglevel>=2) design

    // get n_h
    n_h = J(H,1,.)
    for (h=1;h<=H;h++) {
       this_h = !( J( nobs, 1, h ) - design[.,1] )
       st_select(d_h=., design, this_h )
       n_h[h] = rows(d_h)
    }

    // get m_h
    m_h = J(H,1,.)
    for (h=1;h<=H;h++) {
       this_h = !( J( nobs, 1, h ) - design[.,1] )
       st_select(d_h=., design, this_h )
       m_h[h] = d_h[1,3]
    }

    if (loglevel >= 2) ( runningsum(J(H,1,1)),n_h, m_h )

    // length of the Halton sequence
    L = colsum(m_h) * reps * average

    // the base sequence
    Hseq = J(1,2,0) \ halton(L-1,2)

    if (shuffle=="shuffle") {
       // shuffle one of the dimensions
       Hseq = jumble( Hseq[.,1] ) , Hseq[.,2]
//       printf("{res}Shuffled!\n")
    }

    // got the 2D picture, allocate the counts
    // the weight variables
    st_view(weight=.,.,tokens(wgt), tokens(first) )
    weight[,] = J(nobs,reps,0)

    if (balance == "balanced") {
      // try to balance the rows/units
      // order the sequence by observations
      _sort( Hseq, 1 )
      start_l = 1
      stop_l = 0
      for(h=1;h<=H;h++) {
         this_h = !( J( nobs, 1, h ) - design[.,1] )

         // get the Halton submatrix for h-th stratum
         start_l = stop_l + 1
         stop_l = start_l + m_h[h]*reps*average - 1
         hseq = Hseq[| start_l, 1 \ stop_l, 2 |]

         // sequence elements -> integers, by the existing range!
         for(k=0;k<rows(hseq); k++ ) {
               hseq[k+1,1] = floor( k/(m_h[h]*reps*average) *n_h[h] + 1)
         }

         // sort by replicates
         _sort( hseq, 2 )

         // index of the element to be taken
         k = 0

         // get subview of weights
         st_select(w_h=.,weight,this_h)

         for(r=1;r<=reps;r++) {
            // cycle over replicates

            // get m_h*average units for each stratum
            for(i=1;i<=m_h[h]*average;i++) {
               // index of the element
               k++

               // the y-coordinate is the unit number
               j = hseq[k,1]
               if (loglevel >=3 ) {
                   printf("{txt}In stratum {res}%3.0f{txt}, {res}%4.0f{txt}-th call", h, k)
                   printf("{txt} maps ({res}%3.0f{txt},{res}%15.0g{txt}) to ", hseq[k,1], hseq[k,2])
                   printf("{txt}[{res}%5.0f{txt}, {res}%5.0f{txt}]\n", j, r)
               }

               // increase counts of the freq weight variable
               w_h[j,r] = w_h[j,r] + 1/average

            } // end of cycling over units
         } // end of cycling over replicates

      } // end of cycling over strata

      if (loglevel>=3) {
          printf("{txt}Overall debug: L = {res}%6.0f{txt}, sum of weights = {res}%6.0f\n", L, sum( weight ) )
      }

    // end of balanced option
    }

    else {
    // cycle over the elements of the Halton matrix
       // just use the raw data
       for(l=1;l<=L;l++) {
          // the x-coordinate is the replicate number
          k = floor(Hseq[l,2]*reps + 1)
          // the y-coordinate is the unit number
          j = floor(Hseq[l,1]*nobs + 1)
          // increase counts of the freq weight variable
          weight[j,k] = weight[j,k] + 1/average
       }
    }

    if (loglevel >= 3) weight

    if (bvar~="") {
       // check the balance conditions
       st_view(balancevar=.,.,tokens(bvar), tokens(first) )
       balancevar[,]=rowsum( weight )
       if (loglevel >= 3) balancevar
       // should be the same number for everybody!
       // send it back to Stata, anyway
    }
}

void BalancedBootstrapDesign(
    string scalar mh,      // the variable giving # units m_h
    string scalar strata,  // the variable that identifies the strata
    string scalar ID,      // the variable that identifies the unit IDs
    string scalar wgt,     // names of the weights variables
    string scalar balance, // tempvar
    string scalar first,   // the first observation in the block
    real scalar average,   // the number of replications to be averaged
    | real scalar loglevel // log level
) {
    // design information
    // # PSU per stratum are allowed to vary
    // strata numbers are 1, ... , H
    st_view(design=.,.,(tokens(strata), tokens(ID), tokens(mh)), tokens(first) )
if (loglevel>=2) design
    nobs = rows(design)
    H = design[ nobs, 1 ]


    // get n_h, m_h
    n_h = J(H,1,.)
    m_h = J(H,1,.)
    for (h=1;h<=H;h++) {
       this_h = !( J( nobs, 1, h ) - design[.,1] )
       st_select(d_h=., design, this_h )
       n_h[h] = rows(d_h)
       m_h[h] = d_h[1,3]
    }

    // number of weight variables
    R = cols(tokens(wgt) )
    // total number of replications necessary
    Rav = R*average


    // weight variables
    st_view(bsw=.,.,tokens(wgt),tokens(first))

    if (loglevel>=1) printf("{txt}Balancing within strata: \n")

    for(h=1;h<=H;h++) {
       // cycle over strata

       // the strata ID
       this_h = !( J( nobs, 1, h ) - design[.,1] )

       // subview the IDs
       st_select(needID=.,design,this_h)

       // the number of resamples to be taken
       if( mod( Rav*m_h[h]/n_h[h], 1 ) ~= 0) {
          printf("{err}Warning: {txt}cannot balance for stratum {res}%3.0f ", h)
          printf("{txt}(n_h = {res}%3.0f{txt}, m_h = {res}%3.0f{txt})\n", n_h[h], m_h[h])
       }
       // make it a REALLY LONG vector
       repID = needID[.,2] # J( ceil( Rav*m_h[h]/n_h[h] ), 1, 1 )
       // shuffle it!
       _jumble( repID )
       // truncate to the length conformable with R
       repID = repID[|1\m_h[h]*Rav|]
       // reshape the identifiers matrix to conform to the weights
       repID = colshape( repID, Rav )
       // select the weights for the current stratum
       st_select(w_h=.,bsw,this_h)

       for(r=1;r<=R;r++) {
         for(i=1;i<=n_h[h];i++) {
            w_h[i,r] = sum( !(repID[.,(r-1)*average+1..r*average] - J(m_h[h],average,needID[i,2]) ) ) :/ average
         } // end of cycle over observations
       } // end of cycle over replications
       if (mreldif( colsum(w_h), J(1,R,m_h[h]))>1e-8*sqrt(R)) {
          // WEIRD ROUNDOFF ERRORS
          printf("{err}Warning: target subsample size %4.0f not achieved in stratum %3.0f\n", m_h[h], h)
          colsum( w_h )
       }
       if (loglevel==1) printf("{txt}.")
       if (loglevel==3) w_h
    } // end of cycle over strata

    // check the balance conditions
    st_view(bal=.,.,tokens(balance), tokens(first) )
    bal[,]=rowsum( bsw )
if (loglevel>=2) (design, bal)

    // should be the same number for everybody!
    // send it back to Stata, anyway

    if (loglevel==1) printf("{txt}\n")

}

real matrix Primes(real scalar n) {

   N = ceil( n * ln(n) + n * ln( ln( n ) ) + 10 )
   vv = range(2,N,1)
   for(k=1;k<=N;k++) {
      kk = floor( sqrt(k) )
      for(i=1;i<=kk & i<=length(vv);i++) {
         p = vv[i]
         if (p*p > k) {
            // k is a prime, keep it, and jump to the next one
            break
         }
         if (mod(k,p) == 0) {
            // found a divisor, exclude k from the list
            minindex( abs( vv - J(rows(vv),1,k) ) , 1, j, w)
            // j is the index of k in vv
            t = vv[1..j-1]
            if (j<rows(vv)) {
               t = t \ vv[j+1..rows(vv)]
            }
            vv = t
            break
         }

      }
   }
   // vv contains the primes

   return(vv[|1,1 \ n,1|])
}


end // of mata


exit


History: v.1.0 11-09-2007 -- some intermediate stuff going from constant m_h to variable m_h
         v.1.2 15-07-2008 -- variable m_h
                             Mata components integrated with bsweights
                             set seed
                             calibrate and verbose
         v.1.3 20-06-2009 -- sortpreserve
         v.1.4 20-06-2009 -- mean bootstrap option
         v.1.5 20-05-2010 -- a better treatment of non-survey data

Variables and locals:
`strata' == strata of svy, renumbered 1 to H if necessary
            (available in the variable `newstrata' in the latter case)
`id'     == PSU of svy
`weight' == pweight of svy
`n_h'    == n_h, # of PSUs in a stratum
`m_h'    == m_h, # of units to recycle
`first'  == the first observation in the strata-PSU block; selectvar for Mata
