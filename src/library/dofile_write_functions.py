""" The module ``src.library.dofile_write_functions`` contains functions
that write python specific data-types into a Stata compatible format such
as local or global macros. This code is used at several places of the project.

"""


def list_to_macro(macro_type, name, list_, comment='', indent=0):
    """ Write the entries of a *list_* to a *macro_type* stata macro.
    The macro name is *name*. If *comment* is provided, it is inserted
    above the macro. If *indent* is provided, the whole macro is indented
    by *indent* spaces.

    """

    # define a string for the indentation
    space = ' ' * indent
    # define a string with the comment
    if comment != '':
        comment_string = \
            space + '* define a {} macro that contains the {} \n'.format(macro_type, comment)
    elif comment == '':
        comment_string = ''

    # define string that contains the actual stata macro
    macro_string = \
        comment_string \
        + space + '{} {} /// \n'.format(macro_type, name)
    for i in list_:
        macro_string = macro_string \
            + space + '{} /// \n'.format(i)
    macro_string = macro_string.rstrip('/// \n') + '\n\n'
    return macro_string


def command_list_to_stata(command_list, comment='', indent=0):
    """ Write each entry of *command_list* as line of a stata
    do-file. If *comment* is provided, it is inserted before the first
    command. If *indent* is provided, the whole macro is indented
    by *indent* spaces.

    """

    # define a string for the indentation
    space = ' ' * indent
    # define a string for the comment if needed
    if comment != '':
        command_string = '*' + comment + '\n'
    elif comment == '':
        command_string = ''
    # add the stata commands
    for i in command_list:
        command_string = command_string + space + i + '\n'
    return command_string


def dict_to_macros(macro_type, dictionary, name_suffix='', indent=0):
    """ Write the content of a python dictionary to a string in the format
    of Stata macros. The keys of the dictionary are the macro names. The values
    of the dictionary are the macro contents. Keys have to be string variables.
    Values can be strings, integers, floats, lists or tuples.

    """
    # define a string for the indentation
    space = ' ' * indent
    # define a empty string to which each macro can be added
    large_string = ''
    # generate a string for each pair of dictionary items that has the form of a stata macro
    for key, val in dictionary.items():
        macro_string = space + '{} {}_{} /// \n'.format(macro_type, str(key), name_suffix)
        if isinstance(val, (str, int, float)):
            macro_string = macro_string + space + str(val) + '\n'
        elif isinstance(val, (list, tuple)):
            for i in val:
                macro_string = macro_string + space + '{} /// \n'.format(i)
        else:
            raise ValueError('The dictionary values must be strings,'
                             + ' integers, floats, lists or tuples.')
        # add each macro to the large_string
        large_string = large_string + macro_string.rstrip('/// \n') + '\n\n'

    return large_string





















