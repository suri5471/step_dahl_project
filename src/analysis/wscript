#! python
import json


def build(ctx):

    ctx(
        features='run_py_script',
        source='dahl_correction_quantities_test.py',
        deps=[
            ctx.path_to(ctx, 'IN_MODEL_CODE', 'dahl_correction_quantities.py')
        ],
    )

    # load dictionaries with information on the estimated models from
    # json files in src/model_specs in order to loop over them for the
    # dependencies and targets.
    with open('src/model_specs/models.json') as j:
        large_model_dict = json.load(j)
    model_list = sorted(list(large_model_dict.keys()))

    with open('src/model_specs/sector_variables.json') as k:
        sec_var_dict = json.load(k)
    sec_vars_list = list(sec_var_dict.keys())

    with open('src/model_specs/independent_variables.json') as m:
        specification_dict = json.load(m)
    specification_list = list(specification_dict.keys())

    with open('src/library/country_information.json') as f:
        country_dict = json.load(f)
    country_list = sorted(list(country_dict.keys()))

    for c in country_list:
        for m in model_list:
            ctx(
                features='run_py_script',
                source='write_dahl.py',
                deps=[
                    ctx.path_to(ctx, "IN_MODEL_SPECS", "bs_rep.json"),
                    ctx.path_to(ctx, "IN_MODEL_SPECS", "independent_variables.json"),
                    ctx.path_to(ctx, "IN_MODEL_SPECS", "models.json"),
                    ctx.path_to(ctx, "IN_MODEL_SPECS", "sector_variables.json"),
                    ctx.path_to(ctx, "LIBRARY", "country_information.json"),
                    ctx.path_to(ctx, "IN_MODEL_CODE", 'dahl_correction_quantities.py'),
                    ctx.path_to(ctx, "LIBRARY", 'dofile_write_functions.py')

                ],
                target=ctx.path_to(
                    ctx, 'OUT_CODE_ANALYSIS', 'dahl_corr_{}_{}.do'.format(m, c)),
                append=[c, m]
                )

    ctx.add_group()

    for c in country_list:

        for m in model_list:
            ctx(
                features='run_do_script',
                source=ctx.path_to(ctx, "OUT_CODE_ANALYSIS", 'dahl_corr_{}_{}.do'.format(m, c)),
                deps=ctx.path_to(ctx, "OUT_DATA", '{}_step_data_ready.dta'.format(c)),
                target=ctx.path_to(ctx, "OUT_ANALYSIS", "dahl_results/dahl_{}_{}.csv".format(c, m))
            )

        # make a copy of the do-file for each country in order to run them in parallel.
        ctx(
            features='subst',
            source='choice_model.do',
            target='choice_model_{}.do'.format(c)
            )

        ctx(
            features='run_do_script',
            source=ctx.path_to(ctx, "OUT_CODE_ANALYSIS", "choice_model_{}.do".format(c)),
            deps=[
                ctx.path_to(ctx, "OUT_DATA", '{}_step_data_ready.dta'.format(c)),
                ctx.path_to(ctx, "OUT_MODEL_SPECS", 'global_indep_macro_bench.do'),
                ctx.path_to(ctx, "OUT_MODEL_SPECS", 'svyset_info.do')
                ],
            target=ctx.path_to(ctx,
                               "OUT_ANALYSIS",
                               "choice_results/mprobit_sec_var_bench_{}.csv".format(c)),
            append=c
            )

        # make a copy of the do-file for each country in order to run them in parallel.
        ctx(
            features='subst',
            source='simple_bench_ols.do',
            target='simple_bench_ols_{}.do'.format(c)
            )

        # run the do-files for the OLS models in parallel
        ctx(
            features='run_do_script',
            source='simple_bench_ols_{}.do'.format(c),
            deps=[
                ctx.path_to(ctx, "OUT_DATA", '{}_step_data_ready.dta'.format(c)),
                ctx.path_to(ctx, "OUT_MODEL_SPECS", 'global_indep_macro_bench.do'),
                ctx.path_to(ctx, "OUT_MODEL_SPECS", 'svyset_info.do')
                ],
            target=ctx.path_to(ctx, 'OUT_ANALYSIS', 'ols_results/ols_bench_{}.csv'.format(c)),
            append=c
            )
