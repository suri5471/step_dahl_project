""" Provide tests for the class DahlCorrectionQuantities.

"""

import numpy as np
from numpy.testing.utils import assert_array_almost_equal
from nose.tools import *
from src.model_code.dahl_correction_quantities import DahlCorrectionQuantities


class MockDahlWithThreeFixChoiceValues(DahlCorrectionQuantities):

    def __init__(self):
        pass

    def choice_values_included(self):
        return [1, 2, 3]


class MockDahlWithOneFixChoiceValue(DahlCorrectionQuantities):

    def __init__(self):
        pass

    def choice_values_included(self):
        return [1]


class MockDahlWithFixProbsIncluded(DahlCorrectionQuantities):

    def __init__(self):
        self._npol = 3

    def probs_included(self):
        return ['prob1', 'prob2']


class TestDahlCorrectionQuantities:

    def setUp(self):

        self._sec_var_info = {'values_taken': [1, 2, 3, 4],
                              'interesting_vars': {'intvar1': 1, 'intvar2': 2}
                              }

        self._choice_value = 2

    def test_probs_generated(self):
        assert_equal(set(DahlCorrectionQuantities.probs_generated(self)),
                     set(['prob1', 'prob2', 'prob3', 'prob4']))

    def test_choice_values_included_1(self):
        self._nprobs = 1
        assert_equal(set(DahlCorrectionQuantities.choice_values_included(self)),
                     set([2])
                     )

    def test_choice_values_included_int(self):
        self._nprobs = 'int'
        assert_equal(set(DahlCorrectionQuantities.choice_values_included(self)),
                     set([1, 2])
                     )

    def test_choice_values_included_all(self):
        self._nprobs = 'all'
        assert_in(set(DahlCorrectionQuantities.choice_values_included(self)),
                  [set([1, 2, 3]), set([1, 2, 4])]
                  )

    def test_choice_values_included_invalid(self):
        self._nprobs = 3
        assert_raises(ValueError,
                      DahlCorrectionQuantities.choice_values_included, self
                      )

    def test_probs_included(self):
        mock = MockDahlWithThreeFixChoiceValues()
        assert_equal(mock.probs_included(), ['prob1', 'prob2', 'prob3'])

    def test_polynomials_first_entry(self):
        mock = MockDahlWithFixProbsIncluded()
        assert_equal(set(mock._polynomials()[0]),
                     set(['prob1_ttpo_2', 'prob1_ttpo_3', 'prob2_ttpo_2', 'prob2_ttpo_3'])
                     )

    def test_polynomials_second_entry(self):
        mock = MockDahlWithFixProbsIncluded()
        gen_list = ['gen prob1_ttpo_2 = prob1^2',
                    'gen prob1_ttpo_3 = prob1^3',
                    'gen prob2_ttpo_2 = prob2^2',
                    'gen prob2_ttpo_3 = prob2^3'
                    ]

        assert_equal(set(mock._polynomials()[1]),
                     set(gen_list))

    def test_interaction_terms_first_entry_three_probs(self):
        mock = MockDahlWithThreeFixChoiceValues()
        assert_equal(set(mock._interaction_terms()[0]),
                     set(['prob1_x_prob2', 'prob1_x_prob3', 'prob2_x_prob3'])
                     )

    def test_interaction_terms_first_entry_one_prob(self):
        mock = MockDahlWithOneFixChoiceValue()
        assert_equal(set(mock._interaction_terms()[0]),
                     set([])
                     )

    def test_interaction_terms_second_entry_three_probs(self):
        mock = MockDahlWithThreeFixChoiceValues()
        gen_list = ['gen prob1_x_prob2 = prob1*prob2',
                    'gen prob1_x_prob3 = prob1*prob3',
                    'gen prob2_x_prob3 = prob2*prob3'
                    ]

        assert_equal(set(mock._interaction_terms()[1]),
                     set(gen_list)
                     )

    def test_interaction_terms_second_entry_one_prob(self):
        mock = MockDahlWithOneFixChoiceValue()
        assert_equal(set(mock._interaction_terms()[1]),
                     set([])
                     )




if __name__ == '__main__':
    from nose.core import runmodule
    runmodule()
