/*
The file choice_model.do estimates the determinants of sector choice with multinomial
probit. The explanatory variables included are the ones from the benchmark specification,
including the exlusion restrictions.

The lists with independent variables are imported from 
bld.src.model_specs.global_indep_macro_bench.do

After the estimation the marginal effects at the median of the sample are
calculated with the margins command.

Finally the results are exported to csv tables in bld.out.analysis.choice_results.

The do-file has to be called with an argument for the country by the waf 
task-generator. As waf appends the name of the do-file as the first argmuent, this
country argument can be accessed in the local macro `2'

The waf task-generator makes a copy of the do-file for each country in order to
run them in parallel. As the margins command is computationally intensive, the parallel
computing implies a significant speed increase.
*/



// Header do-file with path definitions, those end up in global macros 
include project_paths 
set more off 

* give a more readable name to the country-macro
local country `2'
* open a log-file
log using `"${PATH_OUT_ANALYSIS}/choice_results/mprobit_sec_var_bench_`country'.log"', replace

* import the dataset
use `"${PATH_OUT_DATA}/`2'_step_data_ready.dta"', replace 

* include the country specific svyset statement
include `"${PATH_OUT_MODEL_SPECS}/svyset_info"'

* svyset the dataset 
${`country'_svy}

* include a macro with the independent variables
include `"${PATH_OUT_MODEL_SPECS}/global_indep_macro_bench"'

*actually estimate the choice model and store the results
svy: mprobit sec_var $all_indeps_bench , baseoutcome(0) 
eststo mprobit 
foreach x in 0 1 2 3 6 { 
margins, dydx(*) predict(outcome(`x')) post vce(unconditional) /// 
at((median) _continuous /// 
gender==1 intact_family==1 parental_dummy==1 mother_tongue==1 /// 
means_dummy==0 has_spouse==1) 
eststo, title(sector`x') 
estimates restore mprobit 

}
 
eststo drop mprobit 
*export results to a csv table 
esttab using `"${PATH_OUT_ANALYSIS}/choice_results/mprobit_sec_var_bench_`country'.csv"', replace /// 
se mtitles nonumbers nonotes star(* 0.1 ** 0.05 *** 0.01) /// 
varlabels(_cons constant) b(3) se(3) 

log close
