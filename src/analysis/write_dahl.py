""" Generate do-files for the estimation of wage returns on individual characteristics
with Dahl correction.

The module has to be called with arguments for the *country* and the *model* by the
waf task-generator. It outputs a generated do-file that estimates the corrected
returns for all "interesting variables" of the sector variable specified in the model.
The do-files are saved in ``bld.src.analysis``.

First, five string variables with the main code for the do-files are defined:
Then a instance of the DahlCorrectionQuantities class with the parameters from the
*model* and *country* is created in order to get lists with the relevant quantities.

These lists are written to string variables in the format of Stata macros by functions
imported from ``src.library.dofile_write_functions.py`` and the empty fields in the five
strings with Stata code are formatted with the corresponding macros.

1) *open_log* string does exactly what the name suggests

2) *start_string* includes the project paths, cleans the memory from previous runs
of the program and sets the number of replications for the bootstrap

3) *program_string* Defines the actual program. Here the choice model is estimated,
the predicted probabilities are generated as variables and a Dahl corrected earnings
equation is estimated.

4) in *apply_program_string* the program is executed by the bootstrap command for
complex survey data in order to get Standard errors that are adjusted for the
two step procedure.

5) in the *save_results_string*, the estimated results are stored in csv tables in
``bld.out.analysis.dahl_results``.

"""

import json
import sys
from bld.project_paths import project_paths_join as ppj
from src.model_code.dahl_correction_quantities import DahlCorrectionQuantities
from src.library.dofile_write_functions import list_to_macro, command_list_to_stata

# define the unformatted open_log_string
open_log_string = """
// Header do-file with path definitions, those end up in global macros
include project_paths
// This do file in generated automatically by a python script.
// Do not change it. Changes will be overwritten with the next run
// of the python script.
log using `"${{PATH_OUT_ANALYSIS}}/dahl_results/dahl_{model}_{country}.log"', replace

"""


# define the unformatted start string
start_string = """
// Header do-file with path definitions, those end up in global macros
include project_paths
use `"${{PATH_OUT_DATA}}{country}_step_data_ready.dta"', replace
capture program drop dahl_correction
set more off
macro drop _all

* define number of replications for the bootstrap
local num_rep {rep}
"""

# define the unformatted program_string
program_string = """
* define the program
program dahl_correction, eclass
        syntax anything [pw iw] [if] [, NULLOPT]
        tempname b V

{probs_used_mac}

{probs_included_mac}

        * drop these probs before generating them in the next run
        foreach x of local probs_used {{
        capture drop `x'
        }}

        local sel_dep {sec_var}

        local reg_dep {reg_dep}

{reg_ind_mac}

{excl_mac}

        * estimate the choice model
        {cm} `sel_dep' `reg_indep' `exclusion' [`weight' `exp'] `if'
        predict {probs_gen_mac}

{gen_pol_mac}

{gen_int_mac}

        * estimate the regression model
        regress `reg_dep' `reg_indep' `probs_included' [`weight'`exp'] `if'
        * return the results in a matrix
        matrix `b' = e(b)
        local nobs = e(N)
        local f_stat = e(F)
        local r_squared = e(r2)

        ereturn post `b', obs(`nobs')
        ereturn scalar r2 = `r_squared'
        ereturn scalar F = `f_stat'
        end

"""

# define the unformatted apply_program_string
apply_program_string = """
* drop bootstrap weights if they already exist
capture drop bw_*
* drop svyset if it already exists
capture svyset, clear
* correctly svyset the data
{svyset}

* generate the bootstrap replication weights
bsweights bw_, reps(`num_rep') n(-1)
svyset [pw=W_FinSPwt], bsrweight(bw_*) singleunit(centered)
eststo {model}_{reg_dep}_{country}_bs: svy bootstrap _b: dahl_correction now


"""

# define the unformatted save_results_string
save_results_string = """
include project_paths
* save results to a csv table
esttab _all ///
using `"${{PATH_OUT_ANALYSIS}}dahl_results/dahl_{country}_{model}.csv"', replace ///
csv nonumbers nonotes staraux mtitles se(%8.3f) b(%8.3f) r2 ///
star(* 0.1 ** 0.05 *** 0.01) ///
varlabels(_cons constant R-sq Rsquared)
log close
\n\n\n\n
"""

if __name__ == "__main__":

    # load a dictionary that contains all models for which we want to perform dahl correction
    with open(ppj("IN_MODEL_SPECS", "models.json")) as j:
        large_model_dict = json.load(j)

    # load a dictionary with all relevant information on the sector choice variable
    with open(ppj("IN_MODEL_SPECS", "sector_variables.json")) as p:
        large_sec_vars_dict = json.load(p)

    # define shorthands
    country = sys.argv[1]
    model = sys.argv[2]
    sector_variable = large_model_dict[model]['sector_variable']
    reg_dep_list = list(large_sec_vars_dict[sector_variable]['interesting_vars'].keys())

    # create the do-file for each country and model
    with open(ppj("OUT_CODE_ANALYSIS", "dahl_corr_{}_{}.do".format(model, country)), 'w') as do:

        do.write(open_log_string.format(country=country, model=model))
        # write the dahl correction program for each dependent variable to the do-file
        for variable in reg_dep_list:
            # create an instance of DahlCorrectionQuantities
            dahl = DahlCorrectionQuantities(model=model, country=country, reg_dep=variable)

            # define a dictionary with the arguments for list_to_macro that stay the same
            # for all function calls
            args_dict = {'macro_type': 'local', 'indent': 8}

            # add the arguments that are only used to write all_probterms_used to a macro
            used_args = args_dict
            used_args['name'] = 'probs_used'
            used_args['list_'] = dahl.all_probterms_used()
            used_args['comment'] = 'probabilities used throughout the program'

            probs_used = list_to_macro(**used_args)

            # add the arguments that are only used to write all_probterms_included to a macro
            incl_args = args_dict
            incl_args['name'] = 'probs_included'
            incl_args['list_'] = dahl.all_probterms_included()
            incl_args['comment'] = 'probabilities included in the regression'

            probs_included = list_to_macro(**incl_args)

            # add the arguments that are only used to write regression_indep_vars to a macro
            indep_args = args_dict
            indep_args['name'] = 'reg_indep'
            indep_args['list_'] = dahl.regression_indep_vars()
            indep_args['comment'] = 'variables used in the regression model'

            reg_indep_vars = list_to_macro(**indep_args)

            # add the arguments that are only used to write the exclusion restrictions to a macro
            excl_args = args_dict
            excl_args['name'] = 'exclusion'
            excl_args['list_'] = dahl.exclusions()
            excl_args['comment'] = 'exclusion restrictions'

            exclusions = list_to_macro(**excl_args)

            # write the commands to generate the included polynomials
            gen_polynomials = \
                command_list_to_stata(dahl.poly_gen_list(),
                                      'generate polynomials of the probabilities used',
                                      8
                                      )

            # write the commands to generate the included interaction terms
            gen_inter = \
                command_list_to_stata(dahl.inter_gen_list(),
                                      'generate interaction terms of the probs used',
                                      8
                                      )

            # define a dictionary with the formatting parameters for start_string
            start_forms = {'country': dahl.country(),
                           'rep': dahl.bs_rep()}

            # define a dictionary with the formatting parameters for program_string
            prog_forms = {'probs_used_mac': probs_used,
                          'probs_included_mac': probs_included,
                          'reg_ind_mac': reg_indep_vars,
                          'excl_mac': exclusions,
                          'cm': dahl.choice_model(),
                          'probs_gen_mac': ' '.join(dahl.probs_generated()),
                          'gen_pol_mac': gen_polynomials,
                          'gen_int_mac': gen_inter,
                          'sec_var': dahl.sector_variable(),
                          'reg_dep': dahl.reg_dep()}

            # define a dictionary with the formatting parameters for apply_program_string
            apply_forms = {'svyset': dahl.svyset(),
                           'model': dahl.model(),
                           'reg_dep': dahl.reg_dep(),
                           'country': dahl.country()}

            # write the part of the do-file that depends on the model and independent variable
            do.write(start_string.format(**start_forms))
            do.write(program_string.format(**prog_forms))
            do.write(apply_program_string.format(**apply_forms))

        # define a dictionary with the formatting parameters for save_results_string
        save_forms = {'country': dahl.country(),
                      'model': dahl.model()}

        # write the part of the do-file that occurs only once at the end
        do.write(save_results_string.format(**save_forms))
