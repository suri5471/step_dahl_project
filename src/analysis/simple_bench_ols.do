/*
The file simple_bench_ols.do estimates the labor market returns on the individual
characeteristics included in the benchmark specification for three sectors: Informal
wage-work, formal-wage work and self-emlployment. The returns are estimated with OLS,
without correction for selection bias. 

The lists with independent variables are imported from 
bld.src.model_specs.global_indep_macro_bench.do

Finally the results are exported to csv tables in bld.out.analysis.ols_results.

The do-file has to be called with an argument for the country by the waf 
task-generator. As waf appends the name of the do-file as the first argmuent, this
country argument can be accessed in the local macro `2'

The waf task-generator makes a copy of the do-file for each country in order to
run them in parallel. In the case of OLS this is only relevant because it allows
for more convenient looping over countries. Speed gains are negligible.
*/


set more off
// Header do-file with path definitions, those end up in global macros.
include project_paths
* give a more readable name to the country-macro
local country `2'

* open a log-file
log using `"${PATH_OUT_ANALYSIS}/ols_results/ols_bench_`country'.log"', replace

* include the country specific svyset statement
include `"${PATH_OUT_MODEL_SPECS}/svyset_info"'
* include a macro with the independent variables
include `"${PATH_OUT_MODEL_SPECS}/global_indep_macro_bench"'

* import the dataset
use `"${PATH_OUT_DATA}/`country'_step_data_ready.dta"'
* svyset it
${`country'_svy}

* make a regression for each outcome variable and store the results
foreach variable in lwage2 lwage3 lwage6 {
svy: regress `variable' $regression_bench
eststo `variable'_`country'
}

*save the results in a csv table
esttab ///
using `"${PATH_OUT_ANALYSIS}/ols_results/ols_bench_`country'.csv"', replace /// 
csv nonumbers nonotes staraux mtitles se(%8.3f) b(%8.3f) r2 ///
star(* 0.1 ** 0.05 *** 0.01) varlabels(_cons constant R-sq Rsquared)

log close

