""" Define a class that contains all quantities that are needed to perform
a Dahl selectivity correction in Stata.

A short description of the Dahl Correction can be found in Section 4 of the paper.

The most important quantities for the Dahl correction are lists with the probabilities
that are included in the regression as well as their polynomials up to a specified
degree and their interaction terms.

To implement the correction method in Stata, we also need lists with commands to
generate variables with these quantities.

All non-trivial methods of the class are unit-tested in
``src.analysis.dahl_correction_quantities_test.py``

"""
import json
from bld.project_paths import project_paths_join as ppj


class DahlCorrectionQuantities:
    """ This class defines all quantities that are needed to perform a Dahl
    selectivity correction in Stata.

    To create an instance of the class, the following things have to be specified:
    A *model* name that has a corresponding entry in ``src.model_specs.models.json``,
    a *country*, and a *reg_dep*, i.e. a dependent variable for the regression in which Dahl
    correction is applied. *reg_dep* has to correspond to one of the
    "interesting variables" of the sector variable of the model.

    Additionally the class has methods to return some attributes and model specifications
    that are of interest in their own right.

    """

    def __init__(self, model, country, reg_dep):

        # load the number of bootstrap replications
        with open(ppj("IN_MODEL_SPECS", "bs_rep.json")) as k:
            bs_rep = json.load(k)[0]

        # load the dictionary with the country specific svyset information
        with open(ppj("LIBRARY", "country_information.json")) as m:
            country_dict = json.load(m)

        self._model = model
        self._country = country
        self._bs_rep = bs_rep
        self._reg_dep = reg_dep
        self._svyset_info = country_dict[country]['svyset']
        self._set_model_shorthands()
        self._set_independent_variables_shorthands()
        self._set_sector_variable_information_shorthands()

    def _set_model_shorthands(self):
        """Load information on the model from a dictionary in ``src.model_specs``
        and set private class attributes for easier access.

        """

        with open(ppj("IN_MODEL_SPECS", "models.json")) as j:
            model_info_dict = json.load(j)

        self._model_info = model_info_dict[self._model]
        self._sector_variable = self._model_info['sector_variable']
        self._nprobs = self._model_info['nprobs']
        self._choice_model = self._model_info['choice_model']
        self._indep_spec = self._model_info['indep_spec']
        self._npol = self._model_info['npol']
        self._model_description = self._model_info['description']

    def _set_independent_variables_shorthands(self):
        """Load lists with the independent variables for the regression and choice
        model and set private class attributes for easier access.

        """

        # load a dictionarly of lists with the independent variable lists for the different models
        with open(ppj("IN_MODEL_SPECS", "independent_variables.json")) as n:
            indep_vars = json.load(n)

        self._reg_indep_vars = indep_vars[self._indep_spec]['regression']
        self._exclusions = indep_vars[self._indep_spec]['exclusion']

    def _set_sector_variable_information_shorthands(self):
        """Load information on all sector variables used in the project and private
        class attributes for easier access to those that are relevant for the
        current model.

        """
        # load a dictionary with all relevant information on the sector choice variable
        with open(ppj("IN_MODEL_SPECS", "sector_variables.json")) as p:
            large_sec_vars_dict = json.load(p)

        self._sec_var_info = large_sec_vars_dict[self._sector_variable]
        self._choice_value = self._sec_var_info['interesting_vars'][self._reg_dep]

    def bs_rep(self):
        """The number of bootstrap replications """
        return self._bs_rep

    def country(self):
        """ The analyzed country """
        return self._country

    def model(self):
        """ The name of the model """
        return self._model

    def sector_variable(self):
        """ The sector variable used in the model """
        return self._sector_variable

    def reg_dep(self):
        """ The dependent variable for the regression in which Dahl correction is applied. """
        return self._reg_dep

    def regression_indep_vars(self):
        """ A list with independent variables for the regression """
        return self._reg_indep_vars

    def exclusions(self):
        """ A list with exclusion restrictions, i.e. those variables that are included
        in the estimation of the choice model but not in the regression.

        """
        return self._exclusions

    def choice_model(self):
        """ The estimation method for the choice Model. Currently mprobit and mlogit
        are supported.

        """
        return self._choice_model

    def svyset(self):
        """ Information to svyset the data in Stata. More information can be found
        in the `Stata Documentation <http://www.stata.com/manuals13/svy.pdf>`_.

        """
        return self._svyset_info

    def probs_generated(self):
        """ The probabilities that are estimated in the choice model. Some of them
        are included in the regression as dependent variables.

        """
        probs_generated = []
        for value in self._sec_var_info['values_taken']:
            probs_generated.append('prob{}'.format(value))
        return probs_generated

    def choice_values_included(self):
        """ The values of the *sector_variable* that correspond to the sectors
        for which probabilities are included in the regression.

        """
        choice_values_included = []
        if self._nprobs == 1:
            choice_values_included.append(self._choice_value)
        elif self._nprobs in ['int', 'all']:
            int_vars = self._sec_var_info['interesting_vars']
            reg_dep_list = list(int_vars.keys())
            for var in reg_dep_list:
                choice_values_included.append(int_vars[var])
            if self._nprobs == 'all':
                diff = []
                for value in self._sec_var_info['values_taken']:
                    if value not in choice_values_included:
                        diff.append(value)
                if len(diff) >= 2:
                    choice_values_included = choice_values_included + diff[1:]
        else:
            raise ValueError('The number of probabilities to be included is not supported')
        return choice_values_included

    def probs_included(self):
        """ The probabilities that are included in the regression """

        self._choice_values_included = self.choice_values_included()
        probs_included = []
        for value in self._choice_values_included:
            probs_included.append('prob{}'.format(value))
        return probs_included

    def _polynomials(self):
        self._probs_included = self.probs_included()
        polynomials_included = []
        poly_gen_list = []
        for prob in self._probs_included:
            for i in range(self._npol + 1):
                if i >= 2:
                    polynomials_included.append('{}_ttpo_{}'.format(prob, i))
                    poly_gen_list.append('gen {}_ttpo_{} = {}^{}'.format(prob, i, prob, i))
        return polynomials_included, poly_gen_list

    def polynomials_included(self):
        """ A list with the polynomials of probabilities that are included in the
        regression model.
        """
        return self._polynomials()[0]

    def poly_gen_list(self):
        """ A corresponding list with the Stata commands to generate variables
        with the polynomials of the probabilities that are included in the regression
        model.
        """
        return self._polynomials()[1]

    def _interaction_terms(self):

        self._choice_values_included = self.choice_values_included()
        inter_included = []
        inter_gen_list = []
        for i in self._choice_values_included:
            for j in self._choice_values_included:
                if i < j:
                    inter_included.append('prob{}_x_prob{}'.format(i, j))
                    inter_gen_list.append('gen prob{}_x_prob{} = prob{}*prob{}'.format(i, j, i, j))
        return inter_included, inter_gen_list

    def inter_included(self):
        """ A list with the interaction terms of probabilities that are included
        in the regression model.

        """
        return self._interaction_terms()[0]

    def inter_gen_list(self):
        """ A corresponding list with the Stata commands to generate variables
        with the interaction terms of the probabilities that are included in
        the regression model

        """
        return self._interaction_terms()[1]

    def all_probterms_used(self):
        """ A list with all probability terms ever used or generated for the Dahl
        correction. This list is important to drop these terms between two consecutive
        runs of the program.

        """
        return self.probs_generated() + self.polynomials_included() + self.inter_included()

    def all_probterms_included(self):
        """ A list with all probability terms, i.e. probabilities, their polynomials
        and their interaction terms actually used in the regression.

        """
        return self.probs_included() + self.polynomials_included() + self.inter_included()
