\section{Economic Model}
\subsection{Model description}

% Economic Model (similar to Dahl)
The analysis in this paper is based on the following extended Roy Model which is inspired by the labor market model of \cite{Bennett2012} and the Roy model constructed in \cite{Dahl2002}: Consider a developing economy where each individual can chose one out of K different labor market states. In our application these states consist of being inactive, unemployed or an unpaid family worker, an informal wage worker, a formal wage worker or self-employed. Each individual has a vector of characteristics that are rewarded differently in each labor market sector.

Sector k is chosen if it maximizes i's utility which depends on i's earnings in sector k and i's valuation of other sector characteristics (such as variability of income, flexibility of working hours, access to social security etc.):

\begin{equation}
U_{ik} = y_{ik} + t_{ik}, 
\end{equation}

where $y_{ik}$ is i's log hourly income in sector k and $t_{ik}$ are i's taste evaluations of other attributes of sector k. We assume that all individuals know their tastes and the expected value (or long term mean) of their potential hourly income in all sectors. As Bennett et al. we assume that wages are paid with certainty in the formal wage sector, whereas earnings have some degree of uncertainty in the informal wage sector and the self-employed sector. For example, in every pay period an informal wage worker faces the risk of not receiving his salary. Individuals have correct information about this uncertainty such that they can base their sector choice on their risk preferences. Sector choice is thus completely deterministic for the individual. 

The information observed by the researcher differs in two important ways from the information of the individual. Firstly, he does not observe all individual characteristics that influence earnings and tastes and in the two ``risky'' sectors he can not separate the fluctuation from the long term mean. Treating the unobserved factors as random influence, the income equation estimated by the researcher is given by:

\begin{equation} 
y_{ik} = E[y_{ik} \vert x_i] + \epsilon_{ik}, \textnormal{ where } \epsilon_{ik} = \mu_{k} + \eta_{ik} \textnormal{ and } E[y_{ik} \vert x_i] = x_i \beta_k
\end{equation} 

where $y_{ik}$ is as before, $x_i$ is a vector of i's individual characteristics, $\beta_k$ is a vector of the returns to these characteristics in sector k and $\epsilon_{ik}$ is an error term with population mean zero. $\mu_{k}$ is the error component that comes from the uncertainty attached to income in the ``risky'' sectors and $\eta_{ik}$ is the component that comes from the unobserved factors \footnote{Note that $\mu_k$ does not have an $i$ subscript as we consider it a sector specific multiplicative error that results in an additive error in log specification.}. Similarly, the taste equation now has a random error attached to it, say $\omega_{ik}$.

As consequence, the utility function becomes:

\begin{equation}
    U_{ik} = V_{ik} + \psi_{ik}, \textnormal{ where } V_{ik} = E[y_{ik} \vert x_i] + E[t_{ik} \vert x_i] \textnormal{ and } \psi_{ik} = \epsilon_{ik} + \omega_{ik}
\end{equation}

The treatment of unobserved influences as random errors allows us to talk about the ``probability'' that individual i chooses sector k, although the individual makes a completely deterministic decision. \footnote{For a discussion on this interpretation cf. \cite{Train2009}}

Secondly, the researcher can only observe the income of individual i in sector k if k was chosen. However, since $y_{ik}$ and with it $\epsilon_{ik}$ enter i's utility maximization, the error terms' mean over the subpopulation that has chosen sector k is in general not equal to zero. If this mean is correlated with $x_i$, simple OLS estimates for $\beta_k$ will be biased. 

% Abgrenzung von unserem Modell zu IZA Modell.
Although being inspired by \cite{Bennett2012}, our model differs in two important ways. Firstly, we do not assume that income in the two wage sectors is independent of individual abilities. As a consequence, wages are different for different individuals within one sector and the jobs in the formal wage sector do not have to be rationed. Secondly, in our model, abilities are seen as multidimensional, whereas in \cite{Bennett2012} they are summarized into one scalar.

\subsection{Hypotheses}

From our model we can derive some hypotheses on the effects of certain variables on selection into sectors and earnings in these sectors:

In our model the variability of earnings is a sector specific characteristic, that is highest in the self-employed sector, followed by the informal wage sector. This would suggest that if we control for other factors, risk tolerance is a significant predictor for sector choice. Additionally, it is possible that risk attitude influences earnings, especially in the self-employed sector. 

Concerning formal education, we would expect that more educated individuals get higher wages in all sectors and that they select into the formal wage and self-employed sector. We expect the same patterns for all cognitive skills, such as literacy, numeracy and computer skills. Women, who often need more flexibility in their work, will probably accept wage penalties for that flexibility and should be more likely to work informally or be self-employed. They should also be less likely to be active. 

More conscientious individuals should earn more in all sectors but especially in the formal wage sector. We would also expect them to be more likely to choose this sector. Social networks are important in the informal economy (\cite{Cohen1996}). We would therefore expect individuals with high levels of extraversion to be more likely to work informally and also to earn higher wages in this sector. People with high values of emotional stability should earn higher wages in all sectors and be more likely to be self-employed since they handle stress better and have less self-doubts.

Moreover, we expect grit to be an important predictor of success, especially for the self-employed. Therefore, individuals with high levels of grit should be more likely to be self-employed and earn more, especially if they are self-employed. Since the self-employed have to make more decisions that directly affect their earnings, we would expect the return to being a thoughtful decision maker to be especially high in this sector. Additionally, people who do not trust others (which entails higher scores in the hostile attribution bias) should be more likely to be self-employed and less likely to be informally employed. Lastly, having the means to start one's own business should increase the probability to be self-employed. 