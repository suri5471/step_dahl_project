\section{Analysis}

In this section we discuss our findings for the effects of cognitive skills and personality traits on sector choice and hourly log income in the exemplary case of Laos. Similar analyses could be done for any of the other countries but are omitted here. All estimation results that are not included in the text or the appendix are available upon request. We chose to present Laos here because it had the highest response rate and was the only country that included rural households in the sample (\cite{Bank2014}). 

\subsection{Determinants of Sector Choice}

We first analyze the marginal effects of the multionmial probit model (compare table 1) which we later use to predict the probabilities that enter as selection correction terms. We consider the effects for an individual at the median of our sample. Because dummies do not have medians we chose the modus value in these cases. As a result we consider a childless 35-year-old woman (i.e the gender dummy equals 1) without the means to start a business, that has a partner, speaks only Lao and still lived with both parents at the age of 15, who took interest in her education.

\begin{table}[!htp]
    \caption{Estimated Marginal Effects of Multinomial Probit Model in Laos}
    \label{tab:table_1_lao} 
    \centering
    \begin{adjustbox}{max totalsize={\textwidth}{.95\textheight}}
    \input{../../out/tables/table_1_lao.tex}
    \end{adjustbox}
\end{table}

% ISCED
The person we consider has an ISCED value of one, which means she has only completed primary education. Having completed lower secondary education (i.e. raising ISCED by one) would increase her probabilities to become informally or formally employed while reducing her probability to be self-employed. These effects are intuitive since the self-employment opportunities for women with low education consist often of street vending and other very poorly remunerated jobs. 
% gender
If the woman we consider was a man, the predicted probability to be inactive would decrease by over four percent. Age increases the probability to be self-employed, which suggests that younger people often first try to gather some experience and work to accumulate capital for a own business (cf. \cite{Maloney2004}).

% risk
Risk does not have the effects that we expected since increasing risk tolerance of our individual by one point (on a four point scale) makes her more likely to be formally or informally employed and less likely to be self-employed, although the effects are small and not significant. 
% Big Five
Our findings suggest that personality traits play an important role in determining a person's sector choice: Conscientiousness decreases a person's probability to be inactive, which is in line with our hypothesis, just as the effect that higher values of extraversion increase the predicted probability to be informally employed. While we lack intuitive explanations why a higher value of agreeableness would make a person more likely to be informally employed or inactive, it is easy to see that more agreeable people have a comparable disadvantage at being self-employed which involves more negotiation and conflict. Additionally to the big Five, grit is a valuable predictor of sector choice that's both significant and in line with our predictions, increasing the predicted likelihood of being active and self-employed. 
% cognitive skills
Turning to cognitive skills we find surprisingly weak results: Both literacy and numeracy do not seem to play a role for the sector choice of the person we consider here. The only ``cognitive'' skill that plays a relevant role is computer usage which both reduces the person's probability to be self-employed and increases her chances to work in informal or formal employment. 

% family background
Family characteristics also offer surprisingly weak and unintuitive insights into people's sector choices: For example an increase in the considered person's father's education increases the probability of unemployment or being a family worker and reduces the probability of being self-employed. 

% means dummy
Having the means to start her own business makes the woman we consider nearly 20\% more likely to actually be self-employed while decreasing the probabilities of all other outcomes.

% exclusion restrictions
Lastly we turn to our exclusion restrictions, which are quite often significant and intuitively appealing. Having a comparative educational advantage over other household members significantly increases the considered person's likelihood to participate in the labor market, just as we expected. Interestingly, the sector that gains the most from this reduction is self-employment. The data support our hypothesis for the role of percent\_active: Having relatively more active household members, on whose income one can depend, increases the likelihood to be unemployed. Lastly, the effects of having a partner are mostly as we expected, making it more likely to be looking for work or to be self-employed. Surprisingly it also increases our considered woman's probability to be active.


\subsection{Wage Returns to Skills and Personality Traits}


%__________________BENCHMARK_3prob_4pol________________________________


Table 2 shows the results for our full specification of the earnings regression, including the Big Five, and using Dahl's correction method with the probabilities for the earnings generating sectors, their interactions and polynomials up to degree four. \footnote{For brevity we will refer to this model as the full model with three probabilities from now on.}


\begin{table}[!htp]
    \caption{Estimated Wage Returns in the Full Model in Laos}
    \label{tab:table_2_lao} 
    \centering
    \begin{adjustbox}{max totalsize={\textwidth}{.95\textheight}}
    \input{../../out/tables/table_2_lao.tex}
    \end{adjustbox}
\end{table}



%ISCED + gender
The results show that characteristics are rewarded very differently across sectors. For example completing one more stage of education (which equals increasing ISCED by one) increases earnings for the self-employed by only 4\% but by 50\% for those who are formally employed. We can also verify the gender bias that has been documented for other developing countries. Interestingly this bias is much more pronounced (and significant at the 10\% level) for women who are informally employed than for women who are formally employed or self-employed. The wage penalty varies from -10\% to over -30\%.  

%Risk
The willingness to take risks plays a statistically significant role and has a similar positive impact on wages in all sectors. This is surprising as we expected that risk tolerance is mostly important in the wage determination of the self-employed. 

Looking at the returns to personality traits, our concerns that low variability and the subjectiveness of ratings in our measures  will inflate standard errors seem justified. As a result, most of the coefficients of these variables are not significantly different from zero. However, many have the sign we expected. For example, the coefficient for conscientiousness is positive for all sectors and at least larger for the formally employed than for the informally employed. On the other hand. agreeableness has a surprisingly positive and significant effect on the wages of the self-employed. In line with our earlier reasoning, extraversion seems to positively and strongly affect wages for those who are informally employed. However, the coefficients of emotional stability and grit are often not as we expected and insignificant. One of the few personality traits that is significant at the 10\% level is decision which reflects people's self-assessment of how much they think about decisions they make. However, its effect is contrary to our hypothesis since more spontaneous decision makers seem to be more successful as self-employed.
% cognitive skills
Looking at cognitive skills it is surprising to see that both literacy and numeracy seem to rarely play a role in determining wages. On the other hand, computer skills are highly rewarded in all sectors and statistically significant which is in line with what we had expected. 

% family background
Turning to the family background of the individual we find some very strong effects: Coming from an intact family has a large positive effect on earnings, indicating that parents might transmit important skills which are not covered by our measures. Unexpectedly, the involvement of parents in their children's education (measured by parental dummy) has a negative effect, except for those who work formally. One possible explanation is that parents tend to get more involved in their children's education when children struggle with school.
% Means dummy
One interesting facet of the STEP surveys is that people were asked whether they had the means to start their own business. While, unsurprisingly, self-employed individuals who answered with yes enjoyed higher incomes, it is unexpected that the employed who answered yes earned on average less than those who didn't.
% children
Individuals with children under six years of age who work in the informal sector earn on average a third less for each young child, while for formal employees wages surprisingly increase with children. 

%__________COMPARISON_TO_WITHOUT_BIG_FIVE________________________________________

Because of concerns that the personality traits measured in the big Five could to some extent reflect similar things as the measures for grit, the hostile attribution bias and decision making, we estimated a specification of our regression model without the big Five. As can be seen in table A1 (in the appendix) our results are generally robust, especially for formal employees. Informal employees have the least robust results. For them several coefficients switch signs, with pronounced changes for ISCED, grit and intact family. However, none of these coefficients are statistically significant. For the self-employed we observe changes in the coefficient of ISCED but they stay statistically insignificant. 

%__________________COMPARISON_TO_OTHER_PROBABILITIES_AND_NO_SELECTION_________________

We now analyze how well our selectivity correction is specified by comparing our results to those with other specifications. An overview of the estimated results for the self-employed in these specifications is provided in table 3.

%    \begin{small}
%    \input{../../out/tables/table_3_manual.tex}
%    \end{small}

\begin{table}[!htp]
    \caption{Different Selectivity Correction Methods for Self-employed in Laos}
    \label{tab:table_3_lao} 
    \centering
    \begin{adjustbox}{max totalheight=.95\textheight}
    \input{../../out/tables/table_3_lao.tex}
    \end{adjustbox}
\end{table}


% No selectivity correction
Firstly, we compare the corrected results to a simple OLS estimation of wages in each sector without selectivity correction (available as table A2 in the appendix). The comparison shows a great number of coefficients with very pronounced switches of signs (e.g. the coefficient of ISCED for informally employed or the coefficient of computer for the self-employed). This indicates that sizable self-selection takes place and not accounting for it leads to biased results. Especially interesting are the effects of the selectivity correction on the estimated wage discrimination against women. Looking at the uncorrected results the wage penalties are strongly underestimated and only of relevant magnitude in the informal sector. However correcting for women's choices pervasive and strong wage discrimination becomes visible.

Now that we have some evidence that controlling for self-selection is appropriate, we investigate whether our specification of including the  probabilities of the income generating sectors is empirically justified. To do so we first compare the full model with three probabilities to one that only includes the probability for each person to choose the sector she actually chose (plus polynomials of that choice up to degree four). Table A3 in the appendix displays these results. Excluding the predicted probabilities of individuals to choose another income generating sector changes the results quite strongly with many pronounced sign changes in the coefficients, indicating that more probabilities have to be included.

% all probabilities
Lastly we investigate if our assumption that the probabilities of being unemployed and inactive do not give us important information can be empirically supported. To do so we compare the results of our full model to a model in which we include a fourth probability. \footnote{Since probabilities add up to one we exclude the fifth probability to avoid problems with multicollinearity} Comparing these results (table A4 in the appendix) we find that the results are mostly robust. Exceptions include the coefficients of conscientiousness and decision of informal employees. This can be taken as evidence in favor of our model since we would have expected qualitative changes in the estimates if the probabilities we excluded conveyed important information. 

We also compared results between the full model with different included probabilities when these probabilities were estimated using multinomial logit instead of multinomial probit but the results do not change profoundly. 
