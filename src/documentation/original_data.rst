.. _original_data:

*************
Original data
*************

In the folder *src.original_data*, the STEP (Skills Towards Employment and Productivity) datasets are stored exactly as they were downloaded from the `World Bank Central Microdata Catalog <http://microdata.worldbank.org/index.php/catalog/step>`_ in November 2014. Even the names are unchanged. This way the results can be replicated with a fresh copy of the datasets without changing any code or renaming any file. For each country there are two Stata data-files. One that has the word 'working' in its name and contains the data for individuals that were selected for the detailed questionnaire and reading assessment. The other usually has the word 'household' or 'hh' in the name and contains information on non-selected household members. Both are used for the analysis.

