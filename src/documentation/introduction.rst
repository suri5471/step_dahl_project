.. _introduction:


************
Introduction
************

The aim of the project is to analyze the effects of cognitive and non-cognitive abilities on sector choice and wages in developing countries. For this, we used the project templates provided by Hans-Martin von Gaudecker. The general use of these project templates is described at http://hmgaudecker.github.io/econ-project-templates/. 

The implementation of the project has been guided by the following aims:

    * It should be easy to replicate the results of the paper
    * It should be easy to add other countries when new STEP datasets become available
    * It should be easy to add other model_specifications
    * The implementation of the Dahl correction for selection bias should be as general as possible and not limited to the STEP datasets, or analysis of formal and informal sectors.

The exact implementation and the rationale behind it is described in what follows. The project is divided into three main Steps:

    * data management
    * analysis
    * final

In the data management step the datasets are cleaned and made compatible across countries, mainly by renaming variables with different names in different countries. In the analysis step, the actual estimations are performed. In the final step, the results are formatted as LaTeX tables. 

Directories Used in the Project
###############################

Each of the three steps has a own directory in the src folder of the project with the corresponding name. Furthermore, there are directories that are used in several steps. Here is the complete picture of files in the src directory and the corresponding files in the bld directory, where all final and intermediate results are stored:

``src.original_data`` contains the original datasets

``src.data_management`` contains the code for the data management step. The results of this step are stored in ``bld.out.data``

``src.model_specs`` contains `JSON <http://www.json.org/>`_ files with the model specifications and a python module to translate them into stata macros. The generated stata macros are stored in do-files in ``bld.src.model_specs``

``src.model_code`` contains a python module where the relevant quantities for the Dahl Correction are defined.

``src.analysis`` contains the code where the actual analysis is performed. The results are stored in ``bld.src.analysis`` in the case of generated code and in ``bld.out.analysis_results`` in the case of actual results.

``src.final`` contains code to merge the results of the analysis to one large csv file and code to write results to LaTeX tables. The results are stored ``src.out.tables``.

``src.library`` contains `JSON <http://www.json.org/>`_ files and python code that are used in several stages of the project. 

``src.paper`` contains the actual research paper. We have one .tex document for each chapter that are then included in ``research_paper.tex``.

``src.documentation`` contains all files for this documentation. Generated documentation files are stored in ``bld.src.documentation``.


How to Replicate the Results
############################

To replicate the results one only has to:
    * Make sure that Stata and Anaconda are installed and are added to the PATH. Both steps are nicely explained in this `Tutorial <http://hmgaudecker.github.io/econ-python-environment/index.html>`_ for Windows, Mac and Linux.

    * Make sure a LaTeX distribution (i.e. `Texlive <https://www.tug.org/texlive/>`_) and `Biber <http://biblatex-biber.sourceforge.net/>`_ are installed. For problems with biber see `here <http://hmgaudecker.github.io/econ-project-templates/faq.html#latex-waf>`_.

    * Open a Shell in the root directory of the project and type:
        * python waf.py configure
        * python waf.py (this is the main step and can take rather long)
        * python waf.py install

The stata code was written for version 13.1. However, we didn't include the version statements in the programs, such that it can be run with older versions as well, as long as they support the used commands. This should be the case with version 11.1 and newer.

At the time of writing the code, there was a bug in Stata 13.1 under Windows that led to errors when using the ``mprobit`` and ``mlogit`` commands.  A simple workarround (without any guarantee) seems to be Stata's version control with version 10 before each of these commands. As the rest of our code is not guaranteed to work with version 10, the version should be reset to something higher than 11.1 after the command. The Linux version of Stata doesn't have this problem. The mac version was not checked.

Short Guide to Adding Other Countries
#####################################

    * Add the STEP dataset for the country
    * See if variables have to be renamed and add rename list list to ``src.library.rename_variable.json``. if no variables have to be renamed, add an empty list.
    * add an entry to ``src.library.country_information.json``. The structure of the entries are explained in the documentation of :ref:`library`.
    * Run everything as explained above
    


Short Guide to Adding Other Model Specifications
################################################

    * If the model uses variables that are not yet generated, they have to be generated in ``src.data_management.project_datamanagement.py`` 
    * If the model uses a different list of independent variables, this list has to be added to ``src.model_specs.independent_variables.json`` as explained in :ref:`model_specifications`.
    * If the model uses a different sector_variable, e.g. a different distinction between formal and informal employees, a corresponding entry has to be added to ``src.model_specs.sector_variable.json``.
    * If the model uses a different outcome variable, i.e. annual earnings in a sector instead of hourly wages, the dictionary with the 'interesting_vars' in ``src.model_specs.sector_variables.json`` of the corresponding sector_variable  has to be augmented accordingly
    * Finally an entry in ``src.model_specs.models.json`` has to be added as explained in :ref:`model_specifications`.
    * go to write_tables.py and add what is necessary to export the new results to a LaTeX table.

