.. _model_specifications:

********************
Model specifications
********************

The directory *src.model_specs* contains `JSON <http://www.json.org/>`_ files with model specifications. and a python module to translate them into stata macros. The generated stata macros are stored in do-files in ``bld.src.model_specs``


Number of Bootstrap Replications:
#################################

``bs_rep.json`` contains a list with only one integer valued entry. This entry sets the number of bootstrap replications for the whole project. For test purposes or debugging it is useful to set the bootstrap replications to 2, for the actual estimation it should be between 200 and 500.

Independent Variables:
######################

``independent_variables.json`` contains a dictionary of dictionaries. The keys on the first level are the name of a specification of independent variables. The value of the second level are dictionaries with the keys "regression" and "exclusion". The value that corresponds to "regression" is a list with independent variables for the regression equation. The value that corresponds to "exclusion" is a list with exclusion restrictions for the choice model.

an example of a dictionary in on the second level is:

.. literalinclude:: ../model_specs/independent_variables.json
    :language: json
    :lines: 3-11

Sector Variables:
#################

Sector variable refers to a variable that indicates the sector choice of an individual. Stata requires that this variable only assumes positive integers. ``sector_variables.json`` is again a dictionary of dictionaries. In our case it only has one key on the highest level, but we nevertheless chose this general form in order to make it easy to repeat the analysis with a different way of distinguishing between formal and informal employees. The json file looks as follows:

.. literalinclude:: ../model_specs/sector_variables.json
    :language: json
    :lines: 1-7

``"values_taken"`` in the second level dictionaries indicates which values can be assumed by the sector variable. ``"interesting_vars"`` matches sector specific earnings variables with choice values for sectors. It is called interesting_vars because usually one has partitions of the labor market where the outcome variable is not defined in some sectors and therefore these sectors are uninteresting for the analysis (for example their are no hourly wages for unpaid family workers). 

If one wants to add other outcome Variables than log hourly wages, they just have to be added as interesting variables of the corresponding sector_variable.

Actual Model Specifications:
############################

``models.json`` again contains a dictionary of dictionaries. The keys on the first level are the *model names*. Their values are dictionaries of the following form:

.. literalinclude:: ../model_specs/models.json
    :language: json
    :lines: 3-10

The entry ``"sector_variable"`` specifies the variable that indicates the chosen sector of an individual in the sample. 

``"nprobs"`` indicates how many and which probabilities are included in the regression. So far the program supports the values 1, 'int' and 'all'. If 1 is specified, only the probability for the chosen sector is included in the earnings regression. If 'int' is specified, the probabilities of all "interesting" sectors are included. With interesting sectors we mean those sector where individuals have positive labor incomes, i.e. not the unemployment and inactive sector.

``"choice_model"`` specifies how the choice equations are estimated. So far only the Stata commands 'mprobit' and 'mlogit' are supported, but it would be easy to extend the program to the more general 'asmprobit' command.

``"indep_spec"`` determines which specification of independent variables is used.

``"description"`` is a verbal description of the model.

Translation from json Files to Stata Macros:
############################################

In the python script ``jsons_to_macros.py`` some information from the jsons is translated into Stata macros. This is necessary because Stata cannot appropriately handle json files and has no equivalent to python dictionaries. We only translate some selected parts of the json files to stata macros because this allows as to use arbitrarily complex, i.e. nested dictionaries for the other steps in the analysis. The functions that are used for this purpose are documented in the documentation of :ref:`library`.
