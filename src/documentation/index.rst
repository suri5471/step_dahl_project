.. This the documentation master file for the STEP-Data Dahl Correction Project.
.. You can adapt this file completely to your liking,
.. but it should at least contain the `toctree` directive.


Welcome to the STEP-Data Dahl Correction Project's documentation 
================================================================

.. toctree::
    :maxdepth: 2

    introduction
    original_data
    data_management
    model_specs
    library
    model_code
    analysis
    final
    paper
