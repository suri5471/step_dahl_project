.. _paper:

**************
Research paper
**************

:file:`research_paper.tex` contains the actual paper. For a better structure, most of the text of the paper is written in separate LaTeX documents and than included into research_paper.tex with the ``\input`` command. The names are self-explanatory:

    * :file:`01_introduction.tex`
    * :file:`02_literature_review.tex`
    * :file:`03_our_economic_model.tex`
    * :file:`04_econometric_approach.tex`
    * :file:`05_data.tex`
    * :file:`06_analysis.tex`
    * :file:`07_meta_analysis.tex`
    * :file:`08_conclusions.tex`

We decided against including the formulas from separate LaTeX files as we do not need them for presentations and find the LaTeX document easier to read if the formulas are in it. If we needed formulas in several places of the project, this should be changed.
