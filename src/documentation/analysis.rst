.. _analysis:

************************
Estimation of the Models
************************

The folder ``src.analysis`` contains the code for the estimation of all models. We estimate the wage returns on individual characteristics such as cognitive abilities, personality traits, family background variables and education in different labor market sectors. To control for the selection bias that is caused by a non-random selection into labor market sectors we use different specifications of the Dahl Correction. 
The actual estimation is done with Stata, but the do-files for the Dahl Correction is generated in a Python script. An alternative to writing the whole do-files with Python would have been to write an ado file for the Dahl correction or to only generate some macros with relevant parts of the code in python and include them in manually written do-files. The first method was not chosen because Python offers an easier possibility to test the code. The second method was not chosen because the resulting do files would be relatively hard to read.

Estimation of Returns with Dahl Correction
##########################################

.. automodule:: src.analysis.write_dahl

Estimation of Returns without Correction
########################################

.. literalinclude:: ../analysis/simple_bench_ols.do
    :start-after: /*
    :end-before: */  


Separate Estimation of the Choice Model with Multinomial Probit
###############################################################

.. literalinclude:: ../analysis/choice_model.do
    :start-after: /*
    :end-before: */  
