.. _model_code:

**********
Model code
**********


The Class DahlCorrectionQuantities
##################################

.. automodule:: src.model_code.dahl_correction_quantities

.. autoclass:: src.model_code.dahl_correction_quantities.DahlCorrectionQuantities
    :members: 





