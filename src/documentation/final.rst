.. _final:

************************************
Visualisation and results formatting
************************************


Clean and Merge the Result Tables from Stata
############################################

.. automodule:: src.final.merge_results
    :members:

Actually Genarate The Tables
##############################

.. automodule:: src.final.write_tables
    :members:

.. automodule:: src.final.cross_country_summary_table
    :members:


