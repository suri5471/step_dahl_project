.. _library:

************
Code library
************

The directory *src.library* provides code that may be used by different steps of the analysis. 

Do-File Write Functions
#######################

.. automodule:: src.library.dofile_write_functions
    :members:


Json File With Relevant Variables
#################################

The file ``src.library.relevant_variables.json`` contains a dictionary where the values are lists with the variables from the STEP-Datasets that are kept after the data_managament step. 'work' corresponds to the work-datasets, 'hh' corresponds to the household datasets. The lists contain many more variable than those that are actually used in the analysis. The list was made in an early phase of the project when it was not yet clear which variables will be included. We didn't reduce the list later, because all variables in the list are compatible across countries (i.e. have the same name in all countries) whereas variables that are not in the list can differ between countries. This information could be valuable for someone who wants to add other variants of the model.


Json File With Rename Lists
###########################

The file ``src.library.rename_variable.json`` contains a dictionary with a key for each country + the key 'general'. The values are lists of dictionaries that contain only the original name of a variable in the dataset as a key and the new name as a value. These rename lists are applied to the datasets in the data_management step. The country specific rename lists are only applied to the corresponding country-dataset, whereas the general list is applied to all datasets.


Json File With Country Information
##################################

The file ``src.library.country_information.json`` contains a dictionary with dictionaries for each country under analysis. Each contry specific dictionary has the following form:

.. literalinclude:: ../library/country_information.json
    :language: json
    :lines: 3-9

* The value that corresponds to ``"work"`` is the original name of the work-dataset for the country.
* The value that corresponds to ``"hh"`` is the original name of the household-dataset for the country.
* The value that corresponds to ``"svyset"`` is the Stata command to svyset the dataset of the country. More information on the svyset command in Stata can be found in the `Stata Documentation <http://www.stata.com/manuals13/svy.pdf>`_.
* The value that corresponds to ``"ppp_conv"`` is the conversion factor from the units or currencies in which wages are measured to purchasing power parity US-Dollar Cents. The data comes from the `World Bank <http://data.worldbank.org/indicator/PA.NUS.PRVT.PP>`_. In the Case of Vietnam wages were not measured in the local currency (Dong) but in 1000 Dong. Therefore, the conversion factor was adjusted by this number. 
* The value that corresponds to ``"full_name"`` is the full name of the country. Usually we work with a three letter abbreviation.

