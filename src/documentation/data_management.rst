.. _data_management:

***************
Data management
***************


Documentation of the code in *src.data_management*.

.. automodule:: src.data_management.project_data_management
    :members:


