""" Make the STEP datasets from Bolivia, Ghana, Laos and Vietnam compatible.

Rename variables with different names in different countries.

Define functions to generate new variables that are needed for the analysis
and apply them to the STEP datasets.

Drop variables that are not needed for the analysis to save memory.

Recode variables that have unwanted codes for missing values.

Merge the relevant information from the work- and household datasets.

Write the results to Stata dta datafiles.

"""

import json
import pandas as pd
import numpy as np
import sys
from bld.project_paths import project_paths_join as ppj

# ===========================================================================
# functions for work dataset
# ===========================================================================


def recode_district_in_vietnam(x):
    """Converts the non missing values of of a column of a pandas
    dataframe into integers

    """
    if pd.isnull(x['district']):
        return np.nan
    else:
        return int(x['district'])


def create_for_self_dummy(x):
    """ Returns a dummy variable when applied to a STEP dataset, that
    equals 1 if a person is formally self-employed and 0 else.

    """
    if x['informal_self'] == 0:
        return 1
    else:
        return 0


def create_inf_self_dummy(x):
    """ Returns a dummy variable when applied to a STEP dataset, that
    equals 1 if a person is informally self-employed and 0 else.

    """
    if x['informal_self'] == 1:
        return 1
    else:
        return 0


def create_our_for_self_dummy(x):
    """ Returns an alternative dummy variable for formal self employment
    when applied to a STEP dataset. It equals 1 if a person is
    self-employed and receives social benefits or has used a bank
    loan to found his business

    """
    if x['emp_status'] == 2 and (x['for_self_old'] == 1 or x['m4c_q07'] == 6):
        return 1
    else:
        return 0


def create_our_inf_self_dummy(x):
    """ Returns an alternative dummy variable for informal self
    employment if applied to a STEP dataset. It equals 1 if a person is
    self-employed and does not receive social benefits nor used
    a bank loan to found his business.

    """
    if x['emp_status'] == 2 and x['for_self_old'] != 1:
        return 1
    else:
        return 0


def create_for_wage_dummy(x):
    """ Returns a dummy variable when applied to a STEP dataset that
    equals 1 if a person is an  employee and receives social benefits or
    has a contract with his employer.

    """

    if x['emp_status'] == 1 and (x['benefits'] == 1 or x['m4c_q18'] == 1):
        return 1
    else:
        return 0


def create_inf_wage_dummy(x):
    """ Returns a dummy variable when applied to a STEP dataset that
    equals 1 if a person is an employee but does not receive social
    benefits nor has a contract with his employer.

    """
    if x['emp_status'] == 1 and x['for_wage'] == 0:
        return 1
    else:
        return 0


def create_unemp_dummy(x):
    """ Returns a dummy variable when applied to a STEP dataset, that
    equals 1 if a person is unemployed and 0 else.

    """
    if x['lm_status'] == 2:
        return 1
    else:
        return 0


def create_fam_worker_dummy(x):
    """ Returns a dummy variable when applied to a STEP dataset that
    equals 1 if a person is an unpaid family worker.

    """
    if x['emp_status'] == 3:
        return 1
    else:
        return 0


def create_parental_dummy(x):
    """ Returns a dummy variable when applied to a STEP dataset that
    equals 1 if a person stated that his parents parents kept themselves
    *sometimes* or *always* actively informed of exams, test results or
    grades when they were in primary school.
    """
    if x['parental'] == 1 or x['parental'] == 2:
        return 1
    else:
        return 0


def recode_m2_q45(x):
    """ Recodes the variable m2_q45 to NaN if it equals 9.

    """
    if x['m2_q45'] == 9:
        return np.nan
    else:
        return x['m2_q45']


def correct_ln_earnings(x):
    """ Returns the natural logarithm of the variable *earnings_h* when
    applied to a STEP dataset if hourly earnings are above 1 ppp US-Dollar
    cent and NaN else.

    """
    if x['ppp_earnings_h'] < 1:
        return np.nan
    else:
        return np.log(x['ppp_earnings_h'])


def create_yearly_income(x):
    """ Returns the yearly income in ppp US-Dollars when applied to a STEP
    dataset.

    """
    return x['ppp_earnings_h'] * x['hours'] * 365/(7 * 100)


def create_ln_yearly_income(x):
    """ Returns the natural logarithm of the yearly income if the income is
    larger than zero and NaN else.

    """
    if x['y_wage'] == 0:
        return np.nan
    else:
        return np.log(x['y_wage'])


def create_means_dummy(x):
    """ Returns a dummy variable if applied to a STEP dataset that is equal
    to one if a person has the means to start a business and zero else.

    """
    if pd.isnull(x['m5b_q31_9']):
        return np.nan
    elif x['m5b_q31_9'] == 2:
        return 0
    else:
        return 1


def create_sector_variable(x):
    """ Returns a categorical variable when applied to a STEP dataset
    that is 0 if a person is inactive, 1 if he is unemployed or an unpaid
    family worker, 2 if he is an informal employee, 3 if he is a formal
    employee, 6 if he is self-employed.

    """

    if x['active'] == 0:
        return 0
    elif x['unemp_and_fam'] == 1:
        return 1
    elif x['inf_wage'] == 1:
        return 2
    elif x['for_wage'] == 1:
        return 3
    elif x['inf_self'] == 1:
        return 6
    elif x['for_self'] == 1:
        return 6
    else:
        return np.nan


def create_sector_earnings(x, earnings_variable, sector_variable, sector_value):
    """ Creates a variable that is equal to the specified *earnings_variable* if
    a person chose the sector that corresponds to *sector_value* in the specified
    *sector_variable*.

    """

    if x[sector_variable] == sector_value:
        return x[earnings_variable]
    else:
        return np.nan

# ===========================================================================
# functions for household datasets
# ===========================================================================


def create_our_activity_dummy(x):
    """ Returns a dummy variable that equals zero if a person is younger
    than 15 years or did not work in the last week and did  not look for
    work in the last 4 weeks. It is also zero if the person did not answer
    if she worked in the last week. Else it is equal to 1.

    """
    if x['m1a_q04'] < 15:
        return 0
    elif x['m1a_q14'] == 2 and x['m1a_q15'] == 2:
        return 0
    elif pd.isnull(x['m1a_q14']):
        return 0
    else:
        return 1


def create_member_educ_laos(x):
    """ Returns a variable indicating a person's highest educational level on a
    10 point scale. The variable is missing if the person is under 15 years old
    and 0 if the person has no education. It is 1 if there is no information on
    the person's education level or if she has attended Primary without
    completing a Low diploma, 2 if(s)he completed a Low Diploma in Primary
    school and 3 for Lower Secondary schooling without the Low diploma.
    4 indicates that a Lower Diploma was achieved in Lower Secondary education
    and 5 that Upper Secondary was attended without any diploma. The variable
    is 6 if a Low Diploma was achieved, 7 if (s)he has a Middle diploma and 8
    if (s)he has a High diploma. The variable is 9 if the person has graduated
    from university.

    """
    if x['m1a_q04'] < 15:
        return np.nan
    elif pd.isnull(x['m1a_q09']) or x['m1a_q09'] == 99:
        return 1
    elif x['m1a_q09a'] >= 8 and x['m1a_q09a'] <= 10:
        return 9
    elif x['m1a_q09a'] == 7:
        return 8
    elif x['m1a_q09a'] == 6:
        return 6
    elif x['m1a_q09'] == 4:
        return 5
    elif x['m1a_q09'] == 3:
        if x['m1a_q09a'] != 5:
            return 3
        elif x['m1a_q09a'] == 5:
            return 4
    elif x['m1a_q09'] == 2:
        if x['m1a_q09a'] != 5:
            return 1
        elif x['m1a_q09a'] == 5:
            return 2
    elif x['m1a_q09'] == 1:
        return 0


def create_member_educ_other_countries(x):
    """ Returns a variable indicating a person's educational level on a 9 point
    scale. The variable is missing if the person is under 15 years old and 1 if
    the person has no education. 2 indicates only kindergarten was attended.
    The variable takes values of 3 if the person completed only the first six
    years of primary education and 4 if(s)he attended two more years of
    Primary. A value of 5 signifies that the person has completed secondary
    education and 6 means she has attended some pre-college or higher technical
    courses. The variable is 7 if the person has graduated from university and
    8 if the person has a Ph.D.

    """
    if x['m1a_q04'] < 15:
        return np.nan
    elif pd.isnull(x['m1a_q09']) or x['m1a_q09'] == -9 or x['m1a_q09'] == 99:
        return 1
    else:
        return x['m1a_q09']

# apply the functions if the code is not run for automatic documentation
if __name__ == "__main__":

    country = sys.argv[1]
    # Load a dictionary that contains the names of the original datasets
    with open(ppj("LIBRARY", "country_information.json")) as f:
        country_dict = json.load(f)[country]

    # extract the names of the "work" and "household" datasets
    w_name = country_dict['work']
    hh_name = country_dict['hh']

    # import datasets
    w_df = pd.read_stata(ppj("IN_DATA", w_name), convert_categoricals=False)
    hh_df = pd.read_stata(ppj("IN_DATA", hh_name), convert_categoricals=False)

    # ===========================================================================
    # Data Management for work dataset
    # ===========================================================================

    # load the dictionary that contains list of dictionaries for the variables
    # that have to be renamed in order to make the datasets compatible across
    # countries.
    with open(ppj("LIBRARY", "rename_variables.json")) as f:
        rename_dict = json.load(f)

    # define the country specific rename list
    specific_rename_list = rename_dict[country]

    # rename the variables that have different names in different countries
    for i in specific_rename_list:
        w_df.rename(columns=i, inplace=True)

    # remove heavy outliers
    if country == 'bol':
        w_df = w_df[w_df['PERSID'] != 12000226027]

    if country == 'vie':
        w_df = w_df[w_df['PERSID'] != 63000139001]

    # Drop the variables that will not be needed in the analysis.
    # Load a list with the variables to keep
    with open(ppj("LIBRARY", "relevant_variables.json")) as f:
        relevant_variables_dict = json.load(f)

    # only keep the relevant variables in the work dataset
    relevant_work = relevant_variables_dict["work"]
    w_df = w_df[relevant_work]

    # convert hourly earnings in US-Dollar cent in purchasing power parity
    ppp_conversion_factor = country_dict['ppp_conv']
    w_df['ppp_earnings_h'] = w_df['earnings_h'] * 100 / ppp_conversion_factor

    # recode the district variable in the Vietnamese dataset
    if country == 'vie':
        w_df['district'] = w_df.apply(recode_district_in_vietnam, axis=1)

    # generate variables that are needed for the analysis
    w_df['for_self_old'] = w_df.apply(create_for_self_dummy, axis=1)

    w_df['inf_self_old'] = w_df.apply(create_inf_self_dummy, axis=1)

    w_df['for_self'] = w_df.apply(create_our_for_self_dummy, axis=1)

    w_df['inf_self'] = w_df.apply(create_our_inf_self_dummy, axis=1)

    w_df['for_wage'] = w_df.apply(create_for_wage_dummy, axis=1)

    w_df['inf_wage'] = w_df.apply(create_inf_wage_dummy, axis=1)

    w_df['unemp'] = w_df.apply(create_unemp_dummy, axis=1)

    w_df['fam_worker'] = w_df.apply(create_fam_worker_dummy, axis=1)

    w_df['unemp_and_fam'] = w_df['unemp'] + w_df['fam_worker']

    w_df['parental_dummy'] = w_df.apply(create_parental_dummy, axis=1)

    w_df['subj_school_perf'] = w_df.apply(recode_m2_q45, axis=1)

    w_df['lwage'] = w_df.apply(correct_ln_earnings, axis=1)

    w_df['y_wage'] = w_df.apply(create_yearly_income, axis=1)

    w_df['y_lwage'] = w_df.apply(create_ln_yearly_income, axis=1)

    w_df['means_dummy'] = w_df.apply(create_means_dummy, axis=1)

    w_df['sec_var'] = w_df.apply(create_sector_variable, axis=1)

    for value in [0, 1, 2, 3, 6]:
        w_df['lwage{}'.format(value)] = \
            w_df.apply(lambda x: create_sector_earnings(x,
                                                        earnings_variable='lwage',
                                                        sector_variable='sec_var',
                                                        sector_value=value
                                                        ), axis=1)

        w_df['y_lwage{}'.format(value)] = \
            w_df.apply(lambda x: create_sector_earnings(x,
                                                        earnings_variable='y_lwage',
                                                        sector_variable='sec_var',
                                                        sector_value=value), axis=1)

    # summarize the average of mother_educ and father_educ to parent_educ
    w_df['parent_educ'] = w_df['mother_educ'] * 0.5 + w_df['father_educ'] * 0.5

    # generate household identifiers and merging keys to merge work an hh datasets
    for df in w_df, hh_df:
        # we strip '.0' from idcode to avoid encoding problems in the vietnamese dataset
        df['hh_identifier'] = df.apply(
            lambda x: str(x['cluster']) + '_' + str(x['hhn']), axis=1)
        df['merging_key'] = df.apply(
            lambda x: x['hh_identifier'] + '_' + str(x['idcode']).strip('.0'), axis=1)

    # ===========================================================================
    # Data Management for household dataset
    # ===========================================================================

    hh_df['our_active'] = hh_df.apply(create_our_activity_dummy, axis=1)

    if country == 'lao':
        hh_df['member_educ'] = hh_df.apply(create_member_educ_laos, axis=1)
    else:
        hh_df['member_educ'] = hh_df.apply(create_member_educ_other_countries, axis=1)

    # create a variable that indicates the relative educational attainment
    # of a person within her household. It equals one if the person has the
    # highest education of all household members.
    hh_df['pct_rank_educ'] = (hh_df.groupby('hh_identifier')['member_educ']
                              .rank(ascending=True, pct=True))
    # create a variable that measures the percentage of active household
    # members. Active means 15 years or older and working or looking for work.
    hh_df['percent_active'] = (hh_df.groupby('hh_identifier')['our_active']
                               .transform(np.mean))

    if country == 'vie':
        hh_df.to_stata(ppj("OUT_DATA", "vie_test.dta"))

    # reduce the household dataset to the relevant variables
    relevant_hh = relevant_variables_dict["hh"]
    hh_df = hh_df[relevant_hh]

    # merge the household and work datasets
    df = pd.merge(w_df, hh_df, on='merging_key', how='left')

    # drop singleton clusters in Bolivia.
    if country == 'bol':
        df = df[df['cluster'] != 105]
        df = df[df['cluster'] != 108]
        df = df[df['cluster'] != 261]
        df = df[df['strata'] != 25]

    # give nicer names to some frequently used variables.
    general_rename_list = rename_dict['general']

    for i in general_rename_list:
        df.rename(columns=i, inplace=True)

    # write the datasets to Stata dta files
    df.to_stata(ppj("OUT_DATA", "{}_step_data_ready.dta".format(country)))
