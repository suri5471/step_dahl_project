""" Merge the csv files that were exported from Stata in the analysis step
to one large csv file. This way it is easier to create LaTeX tables that
compare different models.

"""
import pandas as pd
import json
from bld.project_paths import project_paths_join as ppj

with open(ppj("IN_MODEL_SPECS", "models.json")) as j:
    large_model_dict = json.load(j)

with open(ppj("LIBRARY", 'country_information.json')) as f:
    country_dict = json.load(f)
countries = sorted(list(country_dict.keys()))

models = sorted(list(large_model_dict.keys()))


def clean_entries(dataset):
    """ Strip the equal sign and quotation mark that Stata added to each
    entry in the result tables and name the column that contains the explanatory
    variables as *Variables*

    """
    for col in dataset.columns:
        dataset[col] = dataset[col].str.strip('="')
        dataset.rename(columns={col: col.strip('="')}, inplace=True)
    dataset.rename(columns={'': 'Variables'}, inplace=True)
    return dataset


def fill_se_entries(dataframe):
    """ Fill the empty fields in the column *'Variables'* that correspond to
    the rows with the standard errors with the string 'se' + the entry in the
    column 'Variables' from the previous line.

    """
    for i in range(len(dataframe)):
        if str(dataframe['Variables'].iloc[i]) == '':
            dataframe['Variables'].iloc[i] = 'se_' + str(dataframe['Variables'].iloc[i - 1])
    return dataframe


def generate_path_list(country_list, model_list):
    """ Generate a list that contains the paths to all result tables that
    shall be merged.

    """
    dahl_pathstring = ppj("OUT_ANALYSIS", "dahl_results/dahl_{}_{}.csv")
    ols_pathstring = ppj("OUT_ANALYSIS", "ols_results/ols_bench_{}.csv")
    path_list = []
    for country in countries:
        for model in model_list:
            path_list.append(dahl_pathstring.format(country, model))
        path_list.append(ols_pathstring.format(country))
    return path_list


def merge_regression_result_tables(path_list):
    """ Actually merge the tables.

    """
    df = pd.read_csv(path_list[0], header=1)
    df = clean_entries(df)
    df = df[['Variables']]
    df = fill_se_entries(df)
    df.dropna(axis=0, inplace=True)

    for path in path_list:
        new_df = pd.read_csv(path, header=1)
        new_df = clean_entries(new_df)
        new_df = fill_se_entries(new_df)
        new_df.dropna(axis=0, how='all', inplace=True)
        df = pd.merge(df, new_df, how='outer', on='Variables')
    return df


def restore_variables_column(x):
    """ Remove the entry in the column 'Variables' in rows that contain
    standard errors.

    """
    if str(x['Variables_with_se']).startswith('se_'):
        return ''
    else:
        return x['Variables_with_se']


def process_choice_results(country):
    """ Clean the result tables from choice models in *country* and store them
    in a csv table.

    """

    df = pd.read_csv(ppj('OUT_ANALYSIS',
                         'choice_results/mprobit_sec_var_bench_{}.csv'.format(country)), header=1)
    df = clean_entries(df)
    df = fill_se_entries(df)
    df.dropna(how='all', inplace=True)
    df = df[~df['Variables'].str.startswith('se_')]
    df.to_csv(ppj("OUT_TABLES", 'cleaned_choice_results_{}.csv'.format(country)))

if __name__ == "__main__":
    path_list = generate_path_list(country_list=countries, model_list=models)
    merged_df = merge_regression_result_tables(path_list)
    final_df = merged_df[~merged_df['Variables'].str.startswith('prob')]
    final_df = final_df[~final_df['Variables'].str.startswith('se_prob')]
    final_df.rename(columns={'Variables': 'Variables_with_se'}, inplace=True)
    final_df['Variables'] = final_df.apply(lambda x: restore_variables_column(x), axis=1)
    final_df = final_df[final_df['Variables'] != 'N']
    final_df.to_csv(ppj("OUT_TABLES", "merged_results.csv"))

    for country in countries:
        process_choice_results(country)
