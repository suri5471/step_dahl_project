import json


def build(ctx):

    with open('src/model_specs/models.json') as j:
        large_model_dict = json.load(j)
    model_list = sorted(list(large_model_dict.keys()))

    with open('src/library/country_information.json') as f:
        country_dict = json.load(f)
    country_list = sorted(list(country_dict.keys()))

    deps_list = []
    for country in country_list:
        deps_list.append(ctx.path_to(ctx, 'OUT_ANALYSIS', 'ols_results/ols_bench_{}.csv'.format(country)))
        deps_list.append(ctx.path_to(ctx, 'OUT_ANALYSIS', 'choice_results/mprobit_sec_var_bench_{}.csv'.format(country)))
        for model in model_list:
            deps_list.append(
                ctx.path_to(ctx,
                            'OUT_ANALYSIS',
                            'dahl_results/dahl_{}_{}.csv'.format(country, model))
                )

    targets = [ctx.path_to(ctx, 'OUT_TABLES', 'merged_results.csv')]
    for country in country_list:
        targets.append(ctx.path_to(ctx, "OUT_TABLES", 'cleaned_choice_results_{}.csv'.format(country)))

    ctx(
        features='run_py_script',
        source='merge_results.py',
        deps=deps_list,
        target=targets

    )

    dependencies = [ctx.path_to(ctx, 'OUT_TABLES', 'merged_results.csv')]
    for country in country_list:
        dependencies.append(ctx.path_to(ctx, 'OUT_TABLES', 'cleaned_choice_results_{}.csv'.format(country)))

    table_list = \
        ['table_1_{}.tex',
         'table_2_{}.tex',
         'table_3_{}.tex',
         'table_A1_{}.tex',
         'table_A2_{}.tex',
         'table_A3_{}.tex',
         'table_A4_{}.tex']

    targets = []
    for table in table_list:
        for country in country_list:
            formatted_table = table.format(country)
            targets.append(ctx.path_to(ctx, 'OUT_TABLES', formatted_table))

    ctx(
        features='run_py_script',
        source='write_tables.py',
        deps=dependencies,
        target=targets

    )

    ctx(
        features='run_py_script',
        source='cross_country_summary_table.py',
        deps=ctx.path_to(ctx, 'OUT_TABLES', 'merged_results.csv'),
        target=ctx.path_to(ctx, 'OUT_TABLES', 'color_table.tex')
    )
