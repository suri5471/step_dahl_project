""" Create a LaTeX Table that summarizes the results for Bolivia, Ghana,
Laos and Vietnam using a different color for each countriy and representing
the size of the coefficients with different numbers of plus and minus signs.

"""


import pandas as pd
import numpy as np
from bld.project_paths import project_paths_join as ppj
from src.final.write_tables import results_to_latex, recode_variable

pd.set_option('display.max_rows', 100)
pd.set_option('display.max_columns', 200)
pd.set_option('display.max_colwidth', 300)


def generate_mean_variable(x, v_list):
    """ generate a variable that is equal to the mean of all variables in *v_list*"""
    return (x[v_list[0]] + x[v_list[1]] + x[v_list[2]] + x[v_list[3]]) / 4


def generate_star_variable(x, column, color):
    """ Replace the coefficients in *column* by LaTeX code for a sign representation
    in in *color*. Three plus (minus) sign represent a large (negative) coefficient.
    Two plus (minus) signs a moderately (negative) one. One plus (minus) sign a small
    (negative) one and o a coefficiont that is close to zero.
    """
    s = '\\textcolor{{{}}}{{{}}}'
    if x[column] <= - 0.2:
        return s.format(color, '-- -- -- ')
    elif x[column] <= - 0.1:
        return s.format(color, '-- -- ')
    elif x[column] <= -0.02:
        return s.format(color, ' -- ')
    elif x[column] <= 0.02:
        return s.format(color, 'o ')
    elif x[column] <= 0.1:
        return s.format(color, '+ ')
    elif x[column] <= 0.2:
        return s.format(color, '+ + ')
    else:
        return s.format(color, '+ + + ')


if __name__ == "__main__":
    # Note: We write this country_list manually as the table would probably
    # not work or not be appropriate anymore if more countries are added.
    country_list = ['bol', 'gha', 'lao', 'vie']

    # read in the dataset
    df = pd.read_csv(ppj("OUT_TABLES", 'merged_results.csv'))

    # define lists that contain the names of the coefficient columns
    # for the three sectors.
    varinf = []
    for country in country_list:
        varinf.append('m10_lwage2_{}_bs'.format(country))

    varfor = []
    for country in country_list:
        varfor.append('m10_lwage3_{}_bs'.format(country))

    varself = []
    for country in country_list:
        varself.append('m10_lwage6_{}_bs'.format(country))


    # define a list with all columns that will be used
    varlist = ['Variables'] + varinf + varfor + varself

    # reduce the dataste to these columns
    df = df[varlist]
    df.dropna(how='any', inplace=True)

    # convert the entries in the dataset to floats
    for variable in df.columns:
        if not variable.startswith('Variables'):
            df[variable] = df[variable].astype(float)

    # drop the rows that are not needed
    for i in ['N', 'constant', 'R-sq']:
        df = df[df['Variables'] != i]

    # Apply the function generate_mean_variable
    df['Inf Mean'] = df.apply(lambda x: generate_mean_variable(x, varinf), axis=1)
    df['For Mean'] = df.apply(lambda x: generate_mean_variable(x, varfor), axis=1)
    df['Self Mean'] = df.apply(lambda x: generate_mean_variable(x, varself), axis=1)

    # apply the function generate_star_variable with the correct color for each country
    for variable in varinf + varfor + varself:
        if variable.endswith('bol_bs'):
            col = 'black'
        elif variable.endswith('gha_bs'):
            col = 'OliveGreen'
        elif variable.endswith('lao_bs'):
            col = 'blue'
        elif variable.endswith('vie_bs'):
            col = 'BrickRed'
        df[variable] = df.apply(lambda x: generate_star_variable(x, variable, col), axis=1)

    # Add the star representations for the four countries in new variables
    sign_vars = ['Inf Signs', 'For Signs', 'Self Signs']
    varlists = [varinf, varfor, varself]

    for i in range(3):
        df[sign_vars[i]] = ''
        for var in varlists[i]:
            df[sign_vars[i]] = df[sign_vars[i]] + df[var]

    # drop the columns that are not needed anymore
    cols_to_keep = \
        ['Variables', 'Inf Signs', 'Inf Mean', 'For Signs', 'For Mean', 'Self Signs', 'Self Mean']

    df = df[cols_to_keep]

    # replace all underscores by spaces
    df['Variables'] = df.apply(recode_variable, axis=1).copy(deep=True)

    # define the arguments for the results_to_latex function
    rename_dict = {}

    message = \
        "The table shows a comparison of estimated wage returns to characteristics. " \
        + "The ``Mean'' columns contain the non-weighted average coefficient " \
        + "of the return in their respective sector. The colors are: Bolivia " \
        + "= black, Ghana = green, Laos  = blue, Vietnam = red. The symbols in the " \
        + "``sign'' columns represent the coefficients as follows: +++ for coeff. " \
        + "> 0.2, ++ for coeff > 0.1, + for coeff > 0.02, o for 0.02 > coeff > -0.02, " \
        + "(for minus signs analogously)"

    out_path = ppj('OUT_TABLES', 'color_table.tex')

    # write the table
    results_to_latex(df, cols_to_keep, rename_dict, out_path, message, stars=False)
