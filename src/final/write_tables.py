""" Generate LaTeX tables with the results.
In the paper we only include tables for Laos, but the same tables
are generated for all other counties as well.

"""

import pandas as pd
import numpy as np
import json
from bld.project_paths import project_paths_join as ppj


def results_to_latex(original_df, column_list, rename_dict, out_path, message='', stars=True):
    """ Reduce the *original_df* to the columns in *column_list*, rename them
    according to the rename_dict and write the resulting dataframe to a string
    in LaTeX table format. Then add a multicolumn cell with the meanings of
    the asterisks, add a *message* as multicolumn and multirow cell, and write
    the resulting string to a LaTeX document specified in *out_path*.

    """
    if stars is True:
        starstring = '*** = significant at 1 \%, ** = significant at 5 \%, * = significant at 10 \%'
    else:
        starstring = ''
    multi_col_string = '\\multicolumn{{{}}}{{l}}{{{}}} \\\\ \n'
    multi_row_string = '\\multirow{{{}}}{{\\linewidth}}{{{}}}'

    df = original_df[column_list].copy(deep=True)
    for i in rename_dict:
        df.rename(columns=i, inplace=True)
    latex = df.to_latex(na_rep='', index=False, bold_rows=True, escape=False)
    # for some reason the na_rep option in pandas to_latex method has not the
    # desired effect. This is a quick and dirty workarround
    latex = latex.replace(' nan ', '     ')

    if stars is True:
        mult_star = multi_col_string.format(len(column_list), starstring)
    else:
        mult_star = ''
    latex = latex.replace('\\bottomrule',
                          '\\midrule'
                          + mult_star
                          + '\\bottomrule\n')

    if message != '':
        n_rows = int(np.ceil(len(message) / 90))
        extraspace = '\\\\ \n' * (n_rows - 1)
        multirow_message = multi_row_string.format(n_rows, message)
        multi_col_row_message = multi_col_string.format(len(column_list), multirow_message)
        latex = latex.replace('\\bottomrule',
                              multi_col_row_message
                              + extraspace
                              + '\\bottomrule\n')

    with open(out_path, 'w') as texfile:
        texfile.write(latex)


def recode_variable(x):
    """ Replace underscores by spaces in the "Variables" column of a dataframe """
    return str(x['Variables']).replace('_', ' ')


def extract_cols_from_rename_list(rename_list):
    """ Return a list that contains the string variables and all keys
    from the dictionaries in *rename_list*
    """
    cols_list = ['Variables']
    for i in rename_list:
        keys_list = i.keys()
        for key in keys_list:
            cols_list.append(key)
    return cols_list

huge_df = pd.read_csv(ppj("OUT_TABLES", 'merged_results.csv'))

# apply the functions
if __name__ == "__main__":

    with open(ppj("LIBRARY", 'country_information.json')) as f:
        country_dict = json.load(f)
    country_list = sorted(list(country_dict.keys()))

    for c in country_list:
        choice_res = pd.read_csv(ppj("OUT_TABLES", 'cleaned_choice_results_{}.csv'.format(c)))
        df = huge_df.copy(deep=True)
        # replace undescores by spaces in Variables column
        df['Variables'] = df.apply(recode_variable, axis=1).copy(deep=True)
        choice_res['Variables'] = choice_res.apply(recode_variable, axis=1).copy(deep=True)

        # define the arguments for Table 1
        rename_1 = [{'sector0': 'Inactive'}, {'sector1': 'Unemployed'},
                    {'sector2': 'Inf. Wage'}, {'sector3': 'For. Wage'},
                    {'sector6': 'Self'}]

        column_list_1 = extract_cols_from_rename_list(rename_1)

        out_path_1 = ppj("OUT_TABLES", "table_1_{}.tex".format(c))

        message_1 = \
            'The table shows marginal effects for an individual who has the median value' \
            + 'in all continuous characteristics and the modus value in all dummy variables'

        # write Table 1
        results_to_latex(choice_res, column_list_1, rename_1, out_path_1, message_1)

        # define the arguments for Table 2
        rename_2 = \
            [{'m10_lwage2_{}_bs'.format(c): 'Informal Wage'},
             {'m10_lwage3_{}_bs'.format(c): 'Formal Wage'},
             {'m10_lwage6_{}_bs'.format(c): 'Self employed'}]

        column_list_2 = extract_cols_from_rename_list(rename_2)

        out_path_2 = \
            ppj("OUT_TABLES", "table_2_{}.tex".format(c))

        message_2 = \
            'The returns are estimated with OLS, using the Dahl method for seltectivity ' \
            + 'correction based on the three probabilities for sectors with positive earnings, ' \
            + 'their interaction terms and polynomials up to degree four.'

        # write Table 2
        results_to_latex(df, column_list_2, rename_2, out_path_2, message_2)

        # define the arguments for Table 3
        rename_3 = [{'m10_lwage6_{}_bs'.format(c): 'Benchmark'},
                    {'lwage6_{}'.format(c): 'No Correction'},
                    {'m08_lwage6_{}_bs'.format(c): '1 Probability'},
                    {'m12_lwage6_{}_bs'.format(c): 'All Probabilities'}]

        column_list_3 = extract_cols_from_rename_list(rename_3)

        out_path_3 = ppj("OUT_TABLES", "table_3_{}.tex".format(c))

        # write Table 3
        results_to_latex(df, column_list_3, rename_3, out_path_3)

        # define the arguments for Table A1
        rename_A1 = \
            [{'m11_lwage2_{}_bs'.format(c): 'Informal Wage'},
             {'m11_lwage3_{}_bs'.format(c): 'Formal Wage'},
             {'m11_lwage6_{}_bs'.format(c): 'Self employed'}]

        column_list_A1 = extract_cols_from_rename_list(rename_A1)

        out_path_A1 = ppj("OUT_TABLES", "table_A1_{}.tex".format(c))

        message_A1 = \
            'The returns are estimated with OLS, using the Dahl method for selectivity correction based ' \
            + 'on the three probabilities for sectors with positive earnings, their interaction terms ' \
            + 'and polynomials up to degree four.'

        big_five = ['conscientiousness', 'openness', 'extraversion', 'agreeableness', 'stability']
        a1_df = df.copy(deep=True)
        for trait in big_five:
            a1_df = a1_df[a1_df['Variables'] != trait]
        cols = \
            ['m11_lwage2_{}_bs'.format(c),
             'm11_lwage3_{}_bs'.format(c),
             'm11_lwage6_{}_bs'.format(c)]

        a1_df.dropna(subset=cols, how='any', inplace=True)
        # write Table A1

        results_to_latex(a1_df, column_list_A1, rename_A1, out_path_A1, message_A1)

        # define the arguments for Table A2
        rename_A2 = \
            [{'lwage2_{}'.format(c): 'Informal Wage'},
             {'lwage3_{}'.format(c): 'Formal Wage'},
             {'lwage6_{}'.format(c): 'Self employed'}]

        column_list_A2 = extract_cols_from_rename_list(rename_A2)

        out_path_A2 = ppj("OUT_TABLES", "table_A2_{}.tex".format(c))

        message_A2 = 'The returns are estimated with OLS, not accounting for self-selection'

        # write Table A2
        results_to_latex(df, column_list_A2, rename_A2, out_path_A2, message_A2)

        # define the Arguments for Table A3
        rename_A3 = \
            [{'m08_lwage2_{}_bs'.format(c): 'Informal Wage'},
             {'m08_lwage3_{}_bs'.format(c): 'Formal Wage'},
             {'m08_lwage6_{}_bs'.format(c): 'Self employed'}]

        column_list_A3 = extract_cols_from_rename_list(rename_A3)

        out_path_A3 = ppj("OUT_TABLES", "table_A3_{}.tex".format(c))

        message_A3 = \
            'The returns are estimated with OLS, using the Dahl method for selectivity ' \
            + 'correction based on the probability to choose the sector actually chosen and ' \
            + 'its polynomials up to degree four.'

        # write Table A3
        results_to_latex(df, column_list_A3, rename_A3, out_path_A3, message_A3)

        # define the arguments for Table A4
        rename_A4 = \
            [{'m12_lwage2_{}_bs'.format(c): 'Informal Wage'},
             {'m12_lwage3_{}_bs'.format(c): 'Formal Wage'},
             {'m12_lwage6_{}_bs'.format(c): 'Self employed'}]
        out_path_A4 = ppj("OUT_TABLES", "table_A4_{}.tex".format(c))

        column_list_A4 = extract_cols_from_rename_list(rename_A4)

        message_A4 = \
            'The returns are estimated with OLS, using the Dahl method for selectivity ' \
            + 'correction based on all probabilities minus the probability to be inactive, ' \
            + 'their interaction terms and polynomials up to degree four.'

        # write table A4
        results_to_latex(df, column_list_A4, rename_A4, out_path_A4, message_A4)
